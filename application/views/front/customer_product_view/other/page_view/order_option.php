<div id="pnopoi"></div>
<div class="buttons">
    <div id="share"></div>
      <?php if (!empty($row['video'])) { $video = json_decode($row['video'], true); ?>
      <div class="panel panel-default" style="margin-bottom:0px">
			  <div class="panel-heading">
			  	<h3 class="panel-title">Informasi Produk  Digital &amp; Jasa</h3>
			  </div>
			  <div class="panel-body">
	        <iframe controls="2" width="100%" height="330" src="<?php echo $video['video_src']; ?> " frameborder="0"></iframe>
				</div>
			</div>
              
      <?php } ?>
      </div>
</div>
<script>
	$(document).ready(function() {
		$('#share').share({
			networks: ['facebook','whatsapp','telegram','line','twitter'],
			theme: 'square'
		});
	});
</script>
<script>
$(document).ready(function() {
	check_checkbox();
});
function check_checkbox(){
	$('.checkbox input[type="checkbox"]').each(function(){
        if($(this).prop('checked') == true){
			$(this).closest('label').find('.cr-icon').addClass('add');
		}else{
			$(this).closest('label').find('.cr-icon').addClass('remove');
		}
    });
}
</script>