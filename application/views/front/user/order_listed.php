<?php 
	$i = 0;
	$status = ['delivered', 'confirmed_by_user'];
	foreach ($orders as $row1) {
		$i++;
?>
	<tr>
		<td class="image">
			<?php echo $i; ?>
		</td>
		<td class="quantity">
			<?php echo date('d M Y',$row1['sale_datetime']); ?>
		</td>
		<td class="description">
			<?php echo currency($row1['grand_total']); ?>
		</td>
		<td class="order-id">
			<?php 
				$payment_status = json_decode($row1['payment_status'],true); 
				foreach ($payment_status as $dev) {
			?>

			<span class="label label-<?php if($dev['status'] == 'paid'){ ?>success<?php } else { ?>danger<?php } ?>" style="margin:2px;">
			<?php
					if(isset($dev['vendor'])){
						echo $this->crud_model->get_type_name_by_id('vendor', $dev['vendor'], 'display_name').' ('.translate('vendor').') : '.$dev['status'];
					} else if(isset($dev['admin'])) {
						echo translate('admin').' : '.$dev['status'];
					}
			?>
			</span>
			<br>
			<?php
				}
			?>
		</td>
		<td class="order-id">
			<?php 
				$delivery_status = json_decode($row1['delivery_status'],true); 
				$deliv = '';
				foreach ($delivery_status as $dev) {
			?>

			<span class="label label-<?php echo (in_array($dev['status'], $status)) ? 'success' : 'danger' ?>" style="margin:2px;">
			<?php
					if(isset($dev['vendor'])){
						echo $this->crud_model->get_type_name_by_id('vendor', $dev['vendor'], 'display_name').' ('.translate('vendor').') : '.$dev['status'];
					} else if(isset($dev['admin'])) {
						echo translate('admin').' : '.$dev['status'];
					}
					$deliv = $dev['status'];
			?>
			</span>
			<br>
			<?php if(isset($dev['condition'])){ ?>
			<span class="label label-<?php echo (in_array($dev['status'], $status)) ? 'success' : 'danger' ?>" style="margin:2px;">
			    Condition: <?php echo $dev['condition']; ?>
			</span>
			<?php } ?>

			<br>

			<?php if(isset($dev['note'])){ ?>
			<span class="label label-<?php echo (in_array($dev['status'], $status)) ? 'success' : 'danger' ?>" style="margin:2px;">
			    Note: <?php echo $dev['note']; ?>
			</span>
			<?php } ?>

			<br>

			<?php if(isset($dev['user_delivery_time'])){ ?>
			<span class="label label-<?php echo (in_array($dev['status'], $status)) ? 'success' : 'danger' ?>" style="margin:2px;">
			    Time Delivered by User: <?php echo date('d M Y',$dev['user_delivery_time']); ?>
			</span>
			<?php } ?>
			<br>
			<?php
				}
			?>
		</td>
		<td class="add">
			<a class="btn btn-theme btn-theme-xs" href="<?php echo base_url(); ?>home/invoice/<?php echo $row1['sale_id']; ?>"><?php echo translate('invoice');?></a>
		<?php if ($deliv != 'confirmed_by_user') { ?>
			<button type="button" type="button" class="btn btn-theme btn-theme-xs open_status_modal" style="background-color: rgb(45, 104, 45); border-color: rgb(53, 121, 53);" data-toggle="modal" data-target="#statusChange" onclick="set_status(<?php echo $row1['sale_id']; ?>)"><?php echo translate('confirm');?></button>
		<?php } ?>
		</td>
	</tr>                                            
<?php 
	}
?>


<tr class="text-center" style="display:none;" >
	<td id="pagenation_set_links" ><?php echo $this->ajax_pagination->create_links(); ?></td>
</tr>
<!--/end pagination-->


<script>
	$(document).ready(function(){ 
		$('.pagination_box').html($('#pagenation_set_links').html());
	});

	function set_status(id){
		$('#content_body').html('');
    ajax_load('<?=base_url()?>home/profile/order_confirm/'+id,'content_body');
  }
</script>


