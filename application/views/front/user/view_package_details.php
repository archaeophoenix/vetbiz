<div class="modal_wrap">
    <div class="row get_into" id="login">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php
                echo form_open(base_url() . 'home/profile/package_set_info/'.$package_data['package_payment_id'], array(
                    'class' => 'form-login',
                    'method' => 'post',
                    'id' => 'package_set_info'
                ));
            ?>
                <div class="row box_shape" style="box-shadow:none;overflow-wrap: break-word; word-wrap: break-word;">

                    <?php if($package_data['payment_status'] == 'due'){ ?>
                        <div class="title">
                            <?php echo translate('not_available_payment_info');?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <textarea class="form-control" name="payment_details" style="height:200px;" placeholder="<?php echo translate('payment_details');?>"></textarea>
                            </div>
                        </div>
                        <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <span class="btn btn-theme-sm btn-block btn-theme-dark pull-right info_add_btn snbtn">
                                <?php echo translate('save');?>
                            </span>
                        </div> -->
                    <?php } else if($package_data['payment_status'] == 'paid' || $package_data['payment_status'] == 'pending') { ?>
                        <div class="title">
                            <?php echo translate('transaction_info');?>
                        </div>
                    <?php } ?>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <td><?php echo translate('user');?></td>
                                    <td><?php echo $package_data['username']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo translate('amount');?></td>
                                    <td><?php echo currency('','def').$package_data['amount']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo translate('datetime');?> </td>
                                    <td><?php echo date('d M,Y',$package_data['purchase_datetime']); ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo translate('method');?></td>
                                    <td>
                                        <?php 
                                            if($package_data['payment_type'] == 'c2'){
                                                echo 'Twocheckout';
                                            }
                                            else echo $package_data['payment_type']; 
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo translate('details');?></td>
                                    <td><?php echo $package_data['payment_details']; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo translate('status');?></td>
                                    <td><?php echo $package_data['payment_status']; ?></td>
                                </tr>
                            </table>
                        </div>
                        <?php /*if($package_data['payment_status'] != 'paid'){?>
                        <div class="form-group btm_border">
                            <label class="col-md-5 control-label" for="page_name"><?php echo translate('status');?></label>
                            <div class="col-md-7">
                                <label class="checkbox-inline"><input type="checkbox" name="package_payment[payment_status]" value="paid"><?php echo translate('paid');?></label>
                                <input type="hidden" name="package_payment[package_payment_id]" value="<?php echo $package_data['package_payment_id']; ?>">
                                <input type="hidden" name="user[user_id]" value="<?php echo $package_data['user_id']; ?>">
                            </div>
                        </div>
                        <?php }*/ ?>

                        <span class="btn btn-theme-sm btn-block btn-theme-dark pull-right info_add_btn snbtn">
                            <?php echo translate('save');?>
                        </span>

                    </div>

                </div>
            </form>
        </div>
    </div>

</div>
<script>
    function set_html(hide,show){
        $('#'+show).show('fast');
        $('#'+hide).hide('fast');
    }
    window.addEventListener("keydown", checkKeyPressed, false);
    function checkKeyPressed(e) {
        if (e.keyCode == "13") {
            $('.snbtn').click();
        }
    }
    function set_method(now){
        $('.meth').hide('fast');
        $('.meth').find('select').attr('name','method_1');
        var val = $(now).val();
        if(val !== ''){
            $('.'+val).show('fast');
            $('.'+val).find('select').attr('name','method');
        }
    }
    $(document).ready(function(){        
        $('.selectpicker').selectpicker();
    });
</script>
<style>
.g-icon-bg {
background: #ce3e26;
}
.g-bg {
background: #de4c34;
height: 37px;
margin-left: 41px;
width: 166px;
}
.modal_wrap{
    padding: 20px 0px;
}
.get_into hr {
    border: 1px solid #e8e8e8  !important;
    height: 0px !important;
    background-image: none !important;
}
.box_shape2 {
    padding: 15px;
    border: solid 1px #e9e9e9;
    background-color: #ffffff;
    margin: -25px 20px;
}
</style>