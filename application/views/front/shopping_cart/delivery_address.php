
<?php 
  if($this->session->userdata('user_login')== "yes"){ 
    $user       = $this->session->userdata('user_id'); 
    $user_data  = $this->db->get_where('user',array('user_id'=>$user))->row(); 
    $username   = $user_data->username;
    $surname    = $user_data->surname;  
    $email      = $user_data->email; 
    $phone      = $user_data->phone; 
    $address1   = $user_data->address1; 
    $address2   = $user_data->address2; 
    $langlat    = $user_data->langlat; 
    $address    = $address1.$address2;
    $zip        = $user_data->zip; 
  } 
?>

<div class="row ">
    <div class="col-md-6">
        <div class="form-group">
            <input class="form-control required" value="<?php echo $username ;?>" name="firstname" type="text" placeholder="<?php echo translate('first_name');?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <input class="form-control required" value="<?php echo $surname ;?>" name="lastname" type="text" placeholder="<?php echo translate('last_name');?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <select name="city_field" id="user_province" onChange="get_city(this.value,this)" class="selectpicker required"   tabindex="2" data-hide-disabled="true" data-live-search="true" data-toggle="tooltip" >
                <option value=""><?php echo translate('chose_a_province')?></option>
                <?php echo $this->crud_model->select_rajaongkir_area('province');?>
            </select>
        </div> 
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <select name="city_field" id="user_city" onChange="get_subdistrict(this.value,this)" class="selectpicker required"   tabindex="2" data-hide-disabled="true" data-live-search="true" data-toggle="tooltip" >
                <option value=""><?php echo translate('chose_a_city')?></option>
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group" >
            <select class="selectpicker required" id="user_subdistrict" name="subdistrict_field" onChange="get_courier(this.value,this)" tabindex="2" data-hide-disabled="true" data-live-search="true" data-toggle="tooltip">
                <option value=""><?php echo translate('chose_a_subdistrict')?></option>
            </select>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <input class="form-control required address" name="address1" value="<?php echo $address1; ?>" type="text" placeholder="<?php echo translate('address_line_1');?>">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <input class="form-control address" name="address2" value="<?php echo $address2; ?>" type="text" placeholder="<?php echo translate('address_line_2');?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <input class="form-control required"  name="zip" type="text" value="<?php echo $zip; ?>" placeholder="<?php echo translate('postcode/ZIP');?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <input class="form-control required" value="<?php echo $email ;?>" name="email" type="text" placeholder="<?php echo translate('email');?>">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <input class="form-control required" value="<?php echo $phone ;?>" name="phone" type="text" placeholder="<?php echo translate('phone_number');?>">
        </div>
    </div>

    <div id="user_courier">
    </div>

    <div class="col-md-12" style="display:none;">
        <div class="checkbox">
            <label>
                <input type="checkbox"> 
                <?php echo translate('ship_to_different_address_for_invoice');?>
            </label>
        </div>
    </div>


    <div class="col-md-12">
        <span class="btn btn-theme-dark" onclick="load_payments();">
            <?php echo translate('next');?>
        </span>
    </div>

</div>


<input type="hidden" id="first" value="yes"/>

<script type="text/javascript">
    
    function get_city(id){
        get_rajaonkir(base_url+'home/rajaongkir/get_city/'+id, 'user_city');
    }

    function get_subdistrict(id){
        get_rajaonkir(base_url+'home/rajaongkir/get_subdistrict/'+id, 'user_subdistrict');
    }

    function get_courier(id){
        var weight = $("#qty1").val();
        get_rajaonkir(base_url+'home/rajaongkir/get_courier/'+id+'/'+weight, 'user_courier');
    }

    function get_rajaonkir(url,id){
        $.ajax({
            url: url,
            cache: false,
            success: function(response){
                document.getElementById(id).innerHTML = response
                $('.selectpicker').selectpicker('refresh');
            }
        })
    }

    /*function(){
        #grand
    }*/

    

    function update_cart(id){
        var data = id;
        var res = data.split('_');
        console.log(res);
        $.ajax({
            url: base_url + 'home/rajaongkir/shipping_cost/',
            type: 'POST',
            data: {
                    secret_id: res[0],
                    value: res[1]
                    },
            cache: false,
            success: function(response){
                update_calc_cart();
            }
        })
    }
</script>