<section class="page-section invoice">
    <div class="container">
    	<?php
			$sale_details = $this->db->get_where('sale',array('sale_id'=>$sale_id))->result_array();
            // echo "<pre>";print_r($sale_details);die();
		foreach($sale_details as $row){
            // echo "<pre>";print_r($row);die();
            $info = json_decode($row['shipping_address'],true);
            $ven = $this->crud_model->vendors_in_sale($row['sale_id']);
            $vend = $this->crud_model->one('vendor', 'WHERE vendor_id = ' . $ven[0], 'display_name, address1, address2, zip, email, phone');
            $vat = $row['vat'];
            $shipping = $row['shipping'];
		?>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="invoice_body">
                    <div class="invoice-title">
                        <div class="invoice_logo hidden-xs">
                        	<?php
								$home_top_logo = $this->db->get_where('ui_settings',array('type' => 'home_top_logo'))->row()->value;
							?>
							<img src="<?php echo base_url(); ?>uploads/logo_image/logo_<?php echo $home_top_logo; ?>.png" alt="SuperShop" style="max-width: 350px; max-height: 80px;"/>
                        </div>
                        <div class="invoice_info">
                            <?php //if($invoice == "guest") {?>
                            <?php if(!empty($row['guest_id'])) {?>
                            <p style="margin-bottom: 0px;"><b><?php echo translate('guest_id'); ?> # :</b><?php echo $row['guest_id']; ?></p>
                            <?php }?>
                            <p style="margin-bottom: 0px;"><b><?php echo translate('invoice'); ?> # :</b><?php echo $row['sale_code']; ?></p>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <address>
                                <div class="panel panel-bordered-grey shadow-none">
                                    <div class="panel-heading">
                                        <h1 class="panel-title"><?php echo translate('billed_to'); ?></h1>
                                    </div>
                                    <!--List group-->
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td><b><?php echo translate('first_name');?></b></td>
                                                <td><?php echo $info['firstname']; ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?php echo translate('last_name');?></b></td>
                                                <td><?php echo $info['lastname']; ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?php echo translate('phone');?></b></td>
                                                <td><?php echo $info['phone']; ?>  </td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </address>
                        </div>
                        
                        <div class="col-md-6 col-sm-6 col-xs-6 hidden-xs">
                            <address>
                                <div class="panel panel-bordered-grey shadow-none">
                                    <div class="panel-heading">
                                        <h1 class="panel-title"><?php echo translate('payment_detail');?></h1>
                                    </div>
                                    <!--List group-->
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td><b><?php echo translate('payment_status');?></b></td>
                                                <td><i><?php echo translate($this->crud_model->sale_payment_status($row['sale_id'])); ?></i></td>
                                            </tr>
                                            <tr>
                                                <td><b><?php echo translate('payment_method');?></b></td>
                                                <td>
                                                    <?php if($info['payment_type'] == 'c2'){
                                                        echo 'TwoCheckout';
                                                    }else{
                                                        echo ucfirst(str_replace('_', ' ', $info['payment_type'])); 
                                                    }?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><b><?php echo translate('payment_date');?></b></td>
                                                <td><?php echo date('d M, Y',$row['sale_datetime'] );?></td>
                                            </tr>
                                            <tr>
                                                    <td><b><?php echo translate('vendors');?></b></td>
                                                    <td><?php echo $vend['display_name'];?></td>
                                                </tr>
                                                <tr>
                                                    <td><b><?php echo translate('address');?></b></td>
                                                    <td><?php echo $vend['address1'];?></td>
                                                </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </address>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title"><?php echo translate('payment_invoice');?></h1>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th><?php echo translate('no');?></th>
                                        <th><?php echo translate('item');?></th>
                                        <th><?php echo translate('options');?></th>
                                        <th><?php echo translate('expedition');?></th>
                                        <th><?php echo translate('quantity');?></th>
                                        <th><?php echo translate('unit_cost');?></th>
                                        <th><?php echo translate('total');?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?php
                                        $product_details = json_decode($row['product_details'], true);
                                        // print_r($product_details);die();
                                        $i =0;
                                        $total = 0;
                                        // $vat = 0;
                                        // $shipping = 0;
                                        foreach ($product_details as $row1) {
                                            if($this->crud_model->is_added_by('product',$row1['id'],$ven[0])){
                                            $expedition = explode('_', $row1['courier_field']);
                                            $i++;
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row1['name']; ?></td>
                                        <td>
                                            <?php 
                                                $all_o = json_decode($row1['option'],true);
                                                $color = $all_o['color']['value'];
                                                    if($color){
                                            ?>
                                            <div style="background:<?php echo $color; ?>; height:25px; width:25px;" ></div>
                                            <?php
                                                }
                                            ?>
                                            <?php
                                                foreach ($all_o as $l => $op) {
                                                    if($l !== 'color' && $op['value'] !== '' && $op['value'] !== NULL){
                                            ?>
                                                <?php echo $op['title'] ?> : 
                                                <?php 
                                                    if(is_array($va = $op['value'])){ 
                                                        echo $va = join(', ',$va); 
                                                    } else {
                                                        echo $va;
                                                    }
                                                ?>
                                                <br>
                                            <?php
                                                    }
                                                }
                                            ?>
                                        </td>
                                        <td><?php echo strtoupper($expedition[2]) . ' - ' . $expedition[3]; ?>  </td>
                                        <td><?php echo $row1['qty']; ?></td>
                                        <td><?php echo currency('','def').' '.$this->cart->format_number($row1['price']); ?></td>
                                        <td><?php echo currency('','def').' '.$this->cart->format_number($row1['subtotal']); $total += $row1['subtotal']; ?></td>
                                        <?php
                                            // $vat += $row1['tax'];
                                            // $shipping += $row1['shipping'];
                                        ?>
                                    </tr>
                                    <?php
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>

                            <div class="col-lg-6 col-md-6 col-sm-6 pull-right margin-top-20">
                                <div class="panel panel-colorful panel-grey shadow-none">
                                    <table class="table" border="0">
                                        <tbody>
                                            <tr>
                                                <td class="text-right"><b><?php echo translate('sub_total_amount');?></b></td>
                                                <td class="text-right"><?php echo currency('','def').' '.$this->cart->format_number($total); ?></td>
                                            </tr>
                                             <tr>
                                                <td class="text-right"><b><?php echo translate('tax');?></b></td>
                                                <td class="text-right"><?php echo currency('','def').' '.$this->cart->format_number($vat); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><b><?php echo translate('shipping');?></b></td>
                                                <td class="text-right"><?php echo currency('','def').' '.$this->cart->format_number($shipping); ?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><b><?php echo translate('grand_total');?></b></td>
                                                <td class="text-right"><?php echo currency('','def').' '.$this->cart->format_number($total+$vat+$shipping); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div> 
                            </div>  
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6 hidden-xs">
                            <address>
                                <div class="panel panel-bordered-grey shadow-none">
                                    <div class="panel-heading">
                                        <h1 class="panel-title"><?php echo translate('client_information');?></h1>
                                    </div>
                                    <!--List group-->
                                    <table class="table" border="0">
                                        <tbody>
                                            <tr>
                                                <td><b><?php echo translate('address_line_1');?></b></td>
                                                <td><?php echo $info['address1']; ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?php echo translate('address_line_2');?></b></td>
                                                <td><?php echo $info['address2']; ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?php echo translate('zipcode');?></b></td>
                                                <td><?php echo $info['zip']; ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?php echo translate('phone');?></b></td>
                                                <td><?php echo $info['phone']; ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?php echo translate('e-mail');?></b></td>
                                                <td><?php echo $info['email']; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </address>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-6 hidden-xs">
                            <address>
                                <div class="panel panel-bordered-grey shadow-none">
                                    <div class="panel-heading">
                                        <h1 class="panel-title"><?php echo translate('seller_informations');?></h1>
                                    </div>
                                    <!--List group-->
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td><b><?php echo translate('address_line_1');?></b></td>
                                                <td><?php echo $vend['address1']; ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?php echo translate('address_line_2');?></b></td>
                                                <td><?php echo $vend['address2']; ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?php echo translate('zipcode');?></b></td>
                                                <td><?php echo $vend['zip']; ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?php echo translate('phone');?></b></td>
                                                <td><?php echo $vend['phone']; ?></td>
                                            </tr>
                                            <tr>
                                                <td><b><?php echo translate('e-mail');?></b></td>
                                                <td><?php echo $vend['email']; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>    
                                </div>
                            </address>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-10 col-md-offset-1 btn_print hidden-xs" style="margin-top:10px;">
            	<!-- <span class="btn btn-info pull-right" onClick="print_invoice()">
					<?php echo translate('print'); ?>
               	</span> -->
                <?php //if($invoice != "guest") {?>
                <?php if(empty($row['guest_id'])) {?>
                <a class="btn btn-danger pull-right" href="<?=base_url()?>home/profile/part/order_history" style="margin-right: 5px;"><?php echo translate('back_to_profile'); ?></a>
                <?php }?>
            </div>
        </div>
        <?php
			}
		?>
    </div>
</section>
<script>
function print_invoice(){
	window.print();
}
</script>
<style type="text/css">    
    @media print {
        .top-bar{
            display: none !important;
        }
        header{
            display: none !important;
        }
        footer{
            display: none !important;
        }
        .to-top{
            display: none !important;
        }
        .btn_print{
            display: none !important;
        }
        .invoice{
            padding: 0px;
        }
        .table{
            margin:0px;
        }
        address{
            margin-bottom: 0px;
			border:1px solid #fff !important;
        }
    }
</style>

