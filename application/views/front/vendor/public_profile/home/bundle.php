
    <div class="row">
        <div class="col-sm-12">
            <div class="row category-products">
                            <div class="col-sm-12">
                                <h2 class="section-title section-title-lg">
                                    <span>
                                        <span class="thin"> <?php echo translate('product_bundle')?></span>
                                    </span>
                                </h2>
                                <div class="featured-products-carousel">
                                    <div class="owl-carousel-b" id="recently-viewed-carousel">
                                    <?php
                                    $this->db->join('brand','brand.brand_id = product.brand','left');
                                    $this->db->join('category','category.category_id = product.category','left');
                                    $this->db->where('status','ok');
                                    $this->db->where('is_bundle','yes');
                                    $this->db->where('added_by',json_encode(array('type' => 'vendor','id' => $vendor_id)));
                                    $vendor_products_bundle = $this->db->get('product')->result_array();
                                        foreach ($vendor_products_bundle as $products_budle) {
                                            echo $this->html_model->product_box($products_budle, 'grid', '2');
                                        }
                                    ?>
                                    </div>
                                </div>
                            </div>
                        </div>
           
                        
                        <script>
                            $(document).ready(function(){
                                $(".owl-carousel-b").owlCarousel({
                                    autoplay: true,
                                    loop: true,
                                    margin: 30,
                                    dots: false,
                                    nav: true,
                                    navText: [
                                        "<i class='fa fa-angle-left'></i>",
                                        "<i class='fa fa-angle-right'></i>"
                                    ],
                                    responsive: {
                                        0: {items: 2},
                                        479: {items: 2},
                                        768: {items: 2},
                                        991: {items: 4},
                                        1024: {items: 4}
                                    }
                                });
                            });
                        </script>
        </div>
        
    </div>
    
<script>
$(document).ready(function(){
    setTimeout( function(){ 
        set_cat_product_box_height();
    },1000 );
});

function set_cat_product_box_height(){
    var max_img = 0;
    $('.category-products img').each(function(){
        var current_height= parseInt($(this).css('height'));
        if(current_height >= max_img){
            max_img = current_height;
        }
    });
    $('.category-products img').css('height',max_img);
    
    var max_title=0;
    $('.category-products .caption-title').each(function(){
        var current_height= parseInt($(this).css('height'));
        if(current_height >= max_title){
            max_title = current_height;
        }
    });
    $('.category-products .caption-title').css('height',max_title);
$('.caption-title').css('overflow','hidden');
}
</script>