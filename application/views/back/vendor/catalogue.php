<div id="content-container">
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo translate('list_product_(_physical_)_manufacture');?></h1>
	</div>
    <div class="tab-base">
        <div class="panel">
            <div class="panel-body">
                <div class="tab-content">
                    <!-- LIST -->
                    <div class="tab-pane fade active in" style="border:1px solid #ebebeb; border-radius:4px;">
                        <script src="<?php echo base_url(); ?>template/back/plugins/bootstrap-table/extensions/export/bootstrap-table-export.js"></script>
                        <div class="panel-body" id="demo_s">
                            <table id="events-table" class="table table-striped"  data-url="<?php echo base_url(); ?>vendor/product/catalogue_data" data-side-pagination="server" data-pagination="true" data-page-list="[5, 10, 20, 50, 100, 200]"  data-show-refresh="true" data-search="true"  data-show-export="true" >
                                <thead>
                                    <tr>
                                        <!-- <th data-field="image" data-align="right" data-sortable="true">
                                            <?php echo translate('image');?>
                                        </th> -->
                                        <th data-field="title" data-align="left" data-sortable="true">
                                            <?php echo translate('title');?>
                                        </th>
                                        <th data-field="current_stock" data-sortable="true">
                                            <?php echo translate('discount');?>
                                        </th>
                                        <th data-field="publish" data-sortable="false">
                                            <?php echo translate('manufacture');?>
                                        </th>
                                        <th data-field="featured" data-sortable="false">
                                            <?php echo translate('brands');?>
                                        </th>
                                        <th data-field="options" data-sortable="false">
                                            <?php echo translate('city');?>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                        <script type="text/javascript">
                            $(document).ready(function(){
                                $('#events-table').bootstrapTable({
                                    /*
                                    onAll: function (name, args) {
                                        console.log('Event: onAll, data: ', args);
                                    }
                                    onClickRow: function (row) {
                                        $result.text('Event: onClickRow, data: ' + JSON.stringify(row));
                                    },
                                    onDblClickRow: function (row) {
                                        $result.text('Event: onDblClickRow, data: ' + JSON.stringify(row));
                                    },
                                    onSort: function (name, order) {
                                        $result.text('Event: onSort, data: ' + name + ', ' + order);
                                    },
                                    onCheck: function (row) {
                                        $result.text('Event: onCheck, data: ' + JSON.stringify(row));
                                    },
                                    onUncheck: function (row) {
                                        $result.text('Event: onUncheck, data: ' + JSON.stringify(row));
                                    },
                                    onCheckAll: function () {
                                        $result.text('Event: onCheckAll');
                                    },
                                    onUncheckAll: function () {
                                        $result.text('Event: onUncheckAll');
                                    },
                                    onLoadSuccess: function (data) {
                                        $result.text('Event: onLoadSuccess, data: ' + data);
                                    },
                                    onLoadError: function (status) {
                                        $result.text('Event: onLoadError, data: ' + status);
                                    },
                                    onColumnSwitch: function (field, checked) {
                                        $result.text('Event: onSort, data: ' + field + ', ' + checked);
                                    },
                                    onPageChange: function (number, size) {
                                        $result.text('Event: onPageChange, data: ' + number + ', ' + size);
                                    },
                                    onSearch: function (text) {
                                        $result.text('Event: onSearch, data: ' + text);
                                    }
                                    */
                                }).on('all.bs.table', function (e, name, args) {
                                    //alert('1');
                                    //set_switchery();
                                }).on('click-row.bs.table', function (e, row, $element) {
                                    
                                }).on('dbl-click-row.bs.table', function (e, row, $element) {
                                    
                                }).on('sort.bs.table', function (e, name, order) {
                                    
                                }).on('check.bs.table', function (e, row) {
                                    
                                }).on('uncheck.bs.table', function (e, row) {
                                    
                                }).on('check-all.bs.table', function (e) {
                                    
                                }).on('uncheck-all.bs.table', function (e) {
                                    
                                }).on('load-success.bs.table', function (e, data) {
                                    set_switchery();
                                }).on('load-error.bs.table', function (e, status) {
                                    
                                }).on('column-switch.bs.table', function (e, field, checked) {
                                    
                                }).on('page-change.bs.table', function (e, size, number) {
                                    
                                }).on('search.bs.table', function (e, text) {
                                    
                                });
                            });

                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<span id="prod" style="display:none;"></span>
<script>
	var base_url = '<?php echo base_url(); ?>';
	var timer = '<?php $this->benchmark->mark_time(); ?>';
	var user_type = 'vendor';
	var module = 'product';
	// var list_cont_func = 'catalogue_list';
</script>

