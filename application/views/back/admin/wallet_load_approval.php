<div>
    <?php
        echo form_open(base_url() . 'admin/wallet_load/approval_set/'.$wallet_load_id, array(
            'class' => 'form-horizontal',
            'method' => 'post',
            'id' => 'wallet_load_approval',
            'enctype' => 'multipart/form-data'
        ));
    ?>
        <div class="panel-body">
            <?php if($data['payment_details'] == '' || $data['payment_details'] == '[]'){ ?>
                <div class="form-group">
                    <!-- <label class="col-sm-2 control-label" for="demo-hor-1"> </label> -->
                    <div class="col-sm-12 text-center">
                        <h1 style="color:red;"><?php echo translate("no_payment_info_provided"); ?></h1>
                    </div>
                </div>
            <?php } else { ?>

                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <td><?php echo translate('user');?></td>
                                <td><?php echo $data['username']; ?></td>
                            </tr>
                            <tr>
                                <td><?php echo translate('amount');?></td>
                                <td><?php echo currency('','def').$data['amount']; ?></td>
                            </tr>
                            <tr>
                                <td><?php echo translate('datetime');?> </td>
                                <td><?php echo date('d M,Y',$data['timestamp']); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo translate('method');?></td>
                                <td><?php echo ($data['method'] == 'c2') ? 'Twocheckout' : $data['method']; ?>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo translate('details');?></td>
                                <td><?php echo $data['payment_details']; ?></td>
                            </tr>
                            <tr>
                                <td><?php echo translate('status');?></td>
                                <td><?php echo $data['status']; ?></td>
                            </tr>
                        </table>
                    </div>
                    <?php if($data['status'] != 'paid'){?>
                    <div class="form-group btm_border">
                        <label class="col-md-5 control-label" for="page_name"><?php echo translate('status');?></label>
                        <div class="col-md-7">
                            <label class="checkbox-inline"><input type="checkbox" name="approval" value="paid"><?php echo translate('paid');?></label>
                        </div>
                    </div>
                    <?php } ?>

                </div>

            <?php } ?>
        </div>
    </form>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        // $('.enterer').css('display','');
        set_switchery();
    });


    $(document).ready(function() {
        $("form").submit(function(e){
            //return false;
        });
    });
</script>
<div id="reserve"></div>
<script type="text/javascript">
    $(document).ready(function(){
        // $('.enterer').hide();
    });
</script>