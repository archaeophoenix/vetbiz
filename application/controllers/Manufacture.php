<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Manufacture extends CI_Controller
{
    /*  
     *  Developed by: Active IT zone
     *  Date    : 14 July, 2015
     *  Active Supershop eCommerce CMS
     *  http://codecanyon.net/user/activeitezone
     */
    
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('paypal');
        $this->load->library('twoCheckout_Lib');
        $this->load->library('vouguepay');
        $this->load->library('pum');
        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        //$this->crud_model->ip_data();
        $manufacture_system   =  $this->db->get_where('general_settings',array('type' => 'manufacture_system'))->row()->value;
        if($manufacture_system !== 'ok'){
            redirect(base_url(), 'refresh');
        }
    }
    
    /* index of the manufacture. Default: Dashboard; On No Login Session: Back to login page. */
    public function index()
    {
        if ($this->session->userdata('manufacture_login') == 'yes') {
            $page_data['page_name'] = "dashboard";
            $this->load->view('back/index', $page_data);
        } else {
            $page_data['control'] = "manufacture";
            $this->load->view('back/login',$page_data);
        }
    }
    
    function login($para1 = '')
    {
        if ($para1 == 'forget_form') {
            $page_data['control'] = 'manufacture';
            $this->load->view('back/forget_password',$page_data);
        } else if ($para1 == 'forget') {
            
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');         
            if ($this->form_validation->run() == FALSE)
            {
                echo validation_errors();
            }
            else
            {
                $query = $this->db->get_where('manufacture', array(
                    'email' => $this->input->post('email')
                ));
                if ($query->num_rows() > 0) {
                    $manufacture_id         = $query->row()->manufacture_id;
                    $password         = substr(hash('sha512', rand()), 0, 12);
                    $data['password'] = sha1($password);
                    $this->db->where('manufacture_id', $manufacture_id);
                    $this->db->update('manufacture', $data);
                    if ($this->email_model->password_reset_email('manufacture', $manufacture_id, $password)) {
                        echo 'email_sent';
                    } else {
                        echo 'email_not_sent';
                    }
                } else {
                    echo 'email_nay';
                }
            }
        } else {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');
            
            if ($this->form_validation->run() == FALSE)
            {
                echo validation_errors();
            }
            else
            {
                $login_data = $this->db->get_where('manufacture', array(
                    'email' => $this->input->post('email'),
                    'password' => sha1($this->input->post('password'))
                ));
                if ($login_data->num_rows() > 0) {
                    if($login_data->row()->status == 'approved'){
                        foreach ($login_data->result_array() as $row) {
                            $this->session->set_userdata('login', 'yes');
                            $this->session->set_userdata('manufacture_login', 'yes');
                            $this->session->set_userdata('manufacture_id', $row['manufacture_id']);
                            $this->session->set_userdata('manufacture_name', $row['display_name']);
                            $this->session->set_userdata('title', 'manufacture');
                            $this->session->unset_userdata('city');
                            echo 'lets_login';
                        }
                    } else {
                        echo 'unapproved';
                    }
                } else {
                    echo 'login_failed';
                }
            }
        }
    }
    
    
    /* Loging out from manufacture panel */
    function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url() . 'manufacture', 'refresh');
    }
    
    /*Product Sale Comparison Reports*/
    function report($para1 = '', $para2 = '')
    {
        if (!$this->crud_model->manufacture_permission('report')) {
            redirect(base_url() . 'manufacture');
        }
        //$page_data['page_name'] = "report";
        //$physical_system     =  $this->crud_model->get_type_name_by_id('general_settings','68','value');
        //$digital_system      =  $this->crud_model->get_type_name_by_id('general_settings','69','value');
        //if($physical_system !== 'ok' && $digital_system == 'ok'){
        //    $this->db->where('download','ok');
        //}
        //if($physical_system == 'ok' && $digital_system !== 'ok'){
        //    $this->db->where('download',NULL);
        //}
        //if($physical_system !== 'ok' && $digital_system !== 'ok'){
        //    $this->db->where('download','0');
        //}
        //$this->db->where('added_by',json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id'))));
        //$page_data['products']  = $this->db->get('product')->result_array();
        //$this->load->view('back/index', $page_data);
        $page_data['page_name'] = "report";
        $page_data['products']  = $this->db->get('product')->result_array();
        $this->load->view('back/index', $page_data);
    }
    
    /*Product Stock Comparison Reports*/
    function report_stock($para1 = '', $para2 = '')
    {
        if (!$this->crud_model->manufacture_permission('report')) {
            redirect(base_url() . 'manufacture');
        }
        if ($this->crud_model->get_type_name_by_id('general_settings','68','value') !== 'ok') {
            redirect(base_url() . 'admin');
        }
        //$page_data['page_name'] = "report_stock";
        //if ($this->input->post('product')) {
        //    $page_data['product_name'] = $this->crud_model->get_type_name_by_id('product', $this->input->post('product'), 'title');
        //    $page_data['product']      = $this->input->post('product');
        //}
        //$this->load->view('back/index', $page_data);
        $page_data['page_name'] = "report_stock";
        if ($this->input->post('product')) {
            $page_data['product_name'] = $this->crud_model->get_type_name_by_id('product', $this->input->post('product'), 'title');
            $page_data['product']      = $this->input->post('product');
        }
        $this->load->view('back/index', $page_data);
    }
    
    /*Product Wish Comparison Reports*/
    function report_wish($para1 = '', $para2 = '')
    {
        if (!$this->crud_model->manufacture_permission('report')) {
            redirect(base_url() . 'manufacture');
        }
        $page_data['page_name'] = "report_wish";
        $this->load->view('back/index', $page_data);
    }
    
    function report_product($para1 = '', $para2 = '')
    {
        if (!$this->crud_model->manufacture_permission('report')) {
            redirect(base_url() . 'manufacture');
        }
        
        if($para1 == 'filter'){
            
            $from_date = $this->input->get('from_date');
            $to_date = $this->input->get('to_date');
            if(!empty($from_date) && !empty($to_date))
            {
             
                $from_date_arr = explode('/',$from_date);
                $to_date_arr = explode('/',$to_date);
                
                $from_date_str = $from_date_arr[2].'-'.$from_date_arr[1].'-'.$from_date_arr[0];
                $to_date_str = $to_date_arr[2].'-'.$to_date_arr[1].'-'.$to_date_arr[0];
                
                $page_data['from_date'] = $from_date_arr[2].'-'.$from_date_arr[1].'-'.$from_date_arr[0];
                $page_data['to_date'] = $to_date_arr[2].'-'.$to_date_arr[1].'-'.$to_date_arr[0];
                
                $this->db->where('product_by_manufacture',NULL);
                $this->db->where('add_timestamp >=',strtotime($from_date_str));
                $this->db->where('add_timestamp <=',strtotime($to_date_str));
                $product_list = $this->db->get('product')->result_array();
                $page_data['product_list'] = $product_list;
                $this->load->view('back/manufacture/report_product_table',$page_data);
            }
            
        }elseif($para1 == 'filter_export'){
            $from_date = $this->input->get('from_date');
            $to_date = $this->input->get('to_date');
            if(!empty($from_date) && !empty($to_date))
            {
             
                $from_date_arr = explode('/',$from_date);
                $to_date_arr = explode('/',$to_date);
                
                $from_date_str = $from_date_arr[2].'-'.$from_date_arr[1].'-'.$from_date_arr[0];
                $to_date_str = $to_date_arr[2].'-'.$to_date_arr[1].'-'.$to_date_arr[0];
               
                $page_data['from_date'] = $from_date_arr[2].'-'.$from_date_arr[1].'-'.$from_date_arr[0];
                $page_data['to_date'] = $to_date_arr[2].'-'.$to_date_arr[1].'-'.$to_date_arr[0];
                $this->db->where('product_by_manufacture',NULL);
                $this->db->where('add_timestamp >=',strtotime($from_date_str));
                $this->db->where('add_timestamp <=',strtotime($to_date_str));
                $product_list = $this->db->get('product')->result_array();
                 $page_data['product_list'] = $product_list;
                $this->load->view('back/manufacture/report_product_table_export',$page_data);
            }
        }else{
            $page_data['page_name'] = "report_product";
            $this->db->where('product_by_manufacture',NULL);
            //$this->db->select('*,sum(current_stock) as current_stock');
            //$this->db->group_by('manufacture_product_id');
            $product_list = $this->db->get('product')->result_array();
            $page_data['product_list'] = $product_list;
            $this->load->view('back/index', $page_data);
        }
        
    }
    
    function report_product_seller($para1 = '', $para2 = '')
    {
        if (!$this->crud_model->manufacture_permission('report')) {
            redirect(base_url() . 'admin');
        }
        if($para1 == 'filter'){
            
            $from_date = $this->input->get('from_date');
            $to_date = $this->input->get('to_date');
            if(!empty($from_date) && !empty($to_date))
            {
             
            $from_date_arr = explode('/',$from_date);
            $to_date_arr = explode('/',$to_date);
            $from_date_str = $from_date_arr[2].'-'.$from_date_arr[1].'-'.$from_date_arr[0];
            $to_date_str = $to_date_arr[2].'-'.$to_date_arr[1].'-'.$to_date_arr[0];
            $page_data['from_date'] = $from_date_arr[2].'-'.$from_date_arr[1].'-'.$from_date_arr[0];
            $page_data['to_date'] = $to_date_arr[2].'-'.$to_date_arr[1].'-'.$to_date_arr[0];
            
            $this->db->where('product_by_manufacture',NULL);
            $this->db->where('add_timestamp >=',strtotime($from_date_str));
            $this->db->where('add_timestamp <=',strtotime($to_date_str));
            $product_list = $this->db->get('product')->result_array();
            $page_data['product_list'] = $product_list;
                $this->load->view('back/manufacture/report_product_seller_table',$page_data);
            }
            
        }elseif($para1 == 'filter_export'){
            $from_date = $this->input->get('from_date');
            $to_date = $this->input->get('to_date');
            if(!empty($from_date) && !empty($to_date))
            {
             
            $from_date_arr = explode('/',$from_date);
            $to_date_arr = explode('/',$to_date);
            $from_date_str = $from_date_arr[2].'-'.$from_date_arr[1].'-'.$from_date_arr[0];
            $to_date_str = $to_date_arr[2].'-'.$to_date_arr[1].'-'.$to_date_arr[0];
            $page_data['from_date'] = $from_date_arr[2].'-'.$from_date_arr[1].'-'.$from_date_arr[0];
            $page_data['to_date'] = $to_date_arr[2].'-'.$to_date_arr[1].'-'.$to_date_arr[0];
            
            $this->db->where('product_by_manufacture',NULL);
            $this->db->where('add_timestamp >=',strtotime($from_date_str));
            $this->db->where('add_timestamp <=',strtotime($to_date_str));
            $product_list = $this->db->get('product')->result_array();
            $page_data['product_list'] = $product_list;
                $this->load->view('back/manufacture/report_product_seller_table_export',$page_data);
            }
        }else{
            $page_data['page_name'] = "report_product_seller";
            $this->db->where('product_by_manufacture',NULL);
            $product_list = $this->db->get('product')->result_array();
            $page_data['product_list'] = $product_list;
            $this->load->view('back/index', $page_data);
        }
       
    }
    
    function report_product_wishlist($para1 = '', $para2 = '')
    {
        if (!$this->crud_model->manufacture_permission('report')) {
            redirect(base_url() . 'manufacture');
        }
        if($para1 == 'filter'){
            
            $from_date = $this->input->get('from_date');
            $to_date = $this->input->get('to_date');
            if(!empty($from_date) && !empty($to_date))
            {
             $this->db->where('download=',NULL);
             $this->db->where('added_by',json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id'))));
            $product_list = $this->db->get('manufacture_product')->result_array();
            $from_date_arr = explode('/',$from_date);
            $to_date_arr = explode('/',$to_date);
            $page_data['product_list'] = $product_list;
            $page_data['from_date'] = $from_date_arr[2].'-'.$from_date_arr[1].'-'.$from_date_arr[0];
            $page_data['to_date'] = $to_date_arr[2].'-'.$to_date_arr[1].'-'.$to_date_arr[0];
                $this->load->view('back/vendor/report_product_wishlist_table',$page_data);
            }
            
        }elseif($para1 == 'filter_export'){
            $from_date = $this->input->get('from_date');
            $to_date = $this->input->get('to_date');
            if(!empty($from_date) && !empty($to_date))
            {
             $this->db->where('download=',NULL);
             $this->db->where('added_by',json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id'))));
            $product_list = $this->db->get('manufacture_product')->result_array();
            $from_date_arr = explode('/',$from_date);
            $to_date_arr = explode('/',$to_date);
            $page_data['product_list'] = $product_list;
            $page_data['from_date'] = $from_date_arr[2].'-'.$from_date_arr[1].'-'.$from_date_arr[0];
            $page_data['to_date'] = $to_date_arr[2].'-'.$to_date_arr[1].'-'.$to_date_arr[0];
                $this->load->view('back/vendor/report_product_wishlist_table_export',$page_data);
            }
        }else{
             $page_data['page_name'] = "report_product_wishlist";
             
            $product_list = $this->db->get('product')->result_array();
            $page_data['product_list'] = $product_list;
            $this->load->view('back/index', $page_data);
        }
       
    }

    function category($para1 = '', $para2 = '')
    {
        if (!$this->crud_model->manufacture_permission('category')) {
            redirect(base_url() . 'manufacture');
        }
        if ($this->crud_model->get_type_name_by_id('general_settings','68','value') !== 'ok') {
            redirect(base_url() . 'manufacture');
        }
       if ($para1 == 'list') {
            $this->db->order_by('category_id', 'desc');
            $this->db->where('digital=',NULL);
            $page_data['all_categories'] = $this->db->get('category')->result_array();
            $this->load->view('back/admin/category_list', $page_data);
       }
    }

    function stock($para1 = '', $para2 = '')
    {
        if (!$this->crud_model->manufacture_permission('stock')) {
            redirect(base_url() . 'manufacture');
        }
        if ($this->crud_model->get_type_name_by_id('general_settings','68','value') !== 'ok') {
            redirect(base_url() . 'manufacture');
        }
        if ($para1 == 'list') {
            $this->db->order_by('stock_id', 'desc');
            $page_data['all_stock'] = $this->db->get('stock')->result_array();
            $this->load->view('back/manufacture/stock_list', $page_data);
        } elseif ($para1 == 'sub_by_cat') {
            echo $this->crud_model->select_html('sub_category', 'sub_category', 'sub_category_name', 'add', 'demo-chosen-select required', '', 'category', $para2, 'get_product');
        }elseif ($para1 == 'pro_by_sub') {
            echo $this->crud_model->select_html('product', 'product', 'title', 'add', 'demo-chosen-select required', '', 'sub_category', $para2,'get_pro_res');
        }else {
            $page_data['page_name'] = "stock";
            $page_data['all_stock'] = $this->db->get('stock')->result_array();
            $this->load->view('back/index', $page_data);
        }
    }
    
    /* Product add, edit, view, delete, stock increase, decrease, discount */
    function product($para1 = '', $para2 = '', $para3 = '')
    {
        if (!$this->crud_model->manufacture_permission('product')) {
            redirect(base_url() . 'manufacture');
        }
        if ($this->crud_model->get_type_name_by_id('general_settings','68','value') !== 'ok') {
            redirect(base_url() . 'admin');
        }
        if ($para1 == 'do_add') {
            $options = array();
            $requirements = array();
            if ($_FILES["images"]['name'][0] == '') {
                $num_of_imgs = 0;
            } else {
                $num_of_imgs = count($_FILES["images"]['name']);
            }
            $data['title']              = $this->input->post('title');
            $data['category']           = $this->input->post('category');
            $data['description']        = $_POST['description'];
            $data['sub_category']       = $this->input->post('sub_category');
            $data['sale_price']         = $this->input->post('sale_price');
            $data['suggested_price']     = $this->input->post('suggested_price');
            $data['add_timestamp']      = time();
            $data['download']           = NULL;
            $data['featured']           = 'no';
            $data['manufacture_featured']    = 'no';
            $data['is_bundle']          = 'no';
            $data['status']             = 'ok';
            $data['rating_user']        = '[]';
            $data['discount']           = $this->input->post('discount');
            $data['discount_type']      = $this->input->post('discount_type');
            $data['color']              = json_encode($this->input->post('color'));
            $data['num_of_imgs']        = $num_of_imgs;
            $data['current_stock']      = $this->input->post('current_stock');
            $data['front_image']        = 0;
            $additional_fields['name']  = (empty($_POST['ad_field_names'])) ? '' : json_encode($_POST['ad_field_names']);
            $additional_fields['value'] = (empty($_POST['ad_field_values'])) ? '' : json_encode($_POST['ad_field_values']);
            $data['additional_fields']  = json_encode($additional_fields);
            
            $data['brand']              = $this->input->post('brand');
            $data['unit']               = $this->input->post('unit');
            $choice_titles              = $this->input->post('op_title');
            $choice_types               = $this->input->post('op_type');
            $choice_no                  = $this->input->post('op_no');
            $data['added_by']           = json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id')));
            $req_title                  = $this->input->post('req_title');
            $req_desc                   = $_POST['req_desc'];

            if(!empty($choice_titles) && count($choice_titles) > 0){
                foreach ($choice_titles as $i => $row) {
                    $choice_options         = $this->input->post('op_set'.$choice_no[$i]);
                    $options[]              =   array(
                                                    'no' => $choice_no[$i],
                                                    'title' => $choice_titles[$i],
                                                    'name' => 'choice_'.$choice_no[$i],
                                                    'type' => $choice_types[$i],
                                                    'option' => $choice_options
                                                );
                }
            }

            if(!empty($req_title)){
                foreach($req_title as $i => $row){
                    $requirements[] = array('index'=>$i,'field'=>$row,'desc'=>$req_desc[$i]);
                }
            }

            $data['options'] = json_encode($options);
            // $data['demo'] = $this->input->post('demo');
            $data['recipe'] = $this->input->post('recipe');
            $data['eprice'] = $this->input->post('eprice');
            $data['ecatalog'] = $this->input->post('ecatalog');
            $data['requirements'] = json_encode($requirements);

            $data['high_price']     = $this->input->post('high_price');
            $data['high_price_lock']     = $this->input->post('high_price_lock');
            $data['low_price']     = $this->input->post('low_price');
            $data['low_price_lock']     = $this->input->post('low_price_lock');

            /*if (!empty($_FILES['recipe']['name'])) {
                $recipe = $this->crud_model->upload('recipe', 'uploads/recipe', uniqid());
                $data['recipe'] = $recipe['name'];
            }*/

            if ($this->db->get_where('business_settings',array('type' => 'commission_set'))->row()->value == 'no') {
                if($this->crud_model->manufacture_can_add_product($this->session->userdata('manufacture_id'))){
                    $this->db->insert('manufacture_product', $data);
                    $id = $this->db->insert_id();
                    $data2 = $data;
                    unset($data2['sale_price'], $data2['suggested_price'], $data2['manufacture_featured']);
                    $data2['sale_price'] = 0;
                    $data2['purchase_price'] = 0;
                    $data2['manufacture_product_id'] = $id;
                    $data2['vendor_featured'] = 'no';
                    $this->db->insert('product', $data2);
                    $ids = $this->db->insert_id();
                    $this->benchmark->mark_time();
                    
                    $this->crud_model->file_up("images", "manufacture_product", $id, 'multi');
                    $this->crud_model->copy_img_product($id, $ids, $data['num_of_imgs']);
                    // $this->crud_model->file_up("images", "product", $ids, 'multi');
                    
                } else {
                    echo 'already uploaded maximum product';
                }
            }
            elseif ($this->db->get_where('business_settings',array('type' => 'commission_set'))->row()->value == 'yes') {
                $this->db->insert('manufacture_product', $data);
                $id = $this->db->insert_id();
                $data2 = $data;
                unset($data2['sale_price'], $data2['suggested_price'], $data2['manufacture_featured']);
                $data2['sale_price'] = 0;
                $data2['purchase_price'] = 0;
                $data2['manufacture_product_id'] = $id;
                $data2['vendor_featured'] = 'no';
                $this->db->insert('product', $data2);
                $ids = $this->db->insert_id();
                $this->benchmark->mark_time();
                $this->crud_model->file_up("images", "manufacture_product", $id, 'multi');
                $this->crud_model->copy_img_product($id, $ids, $data['num_of_imgs']);
                // $this->crud_model->file_up("images", "product", $ids, 'multi');
            }
            $this->crud_model->set_category_data(0);
            recache();
        } else if ($para1 == "update") {
            $options = array();
            $requirements = array();
            if ($_FILES["images"]['name'][0] == '') {
                $num_of_imgs = 0;
            } else {
                $num_of_imgs = count($_FILES["images"]['name']);
            }
            $num                        = $this->crud_model->get_type_name_by_id('manufacture_product', $para2, 'num_of_imgs');
            $download                   = $this->crud_model->get_type_name_by_id('manufacture_product', $para2, 'download');
            $data['title']              = $this->input->post('title');
            $data['category']           = $this->input->post('category');
            $data['description']        = $_POST['description'];
            $data['sub_category']       = $this->input->post('sub_category');
            $data['sale_price']         = $this->input->post('sale_price');
            $data['suggested_price']     = $this->input->post('suggested_price');
            $data['discount']           = $this->input->post('discount');
            $data['discount_type']      = $this->input->post('discount_type');
            $data['color']              = json_encode($this->input->post('color'));
            $data['num_of_imgs']        = $num + $num_of_imgs;
            $data['front_image']        = 0;
            $additional_fields['name']  = (empty($_POST['ad_field_names'])) ? '' : json_encode($_POST['ad_field_names']);
            $additional_fields['value'] = (empty($_POST['ad_field_values'])) ? '' : json_encode($_POST['ad_field_values']);
            $data['additional_fields']  = json_encode($additional_fields);
            $data['brand']              = $this->input->post('brand');
            $data['unit']               = $this->input->post('unit');
            $choice_titles              = $this->input->post('op_title');
            $choice_types               = $this->input->post('op_type');
            $choice_no                  = $this->input->post('op_no');
            $req_title                  = $this->input->post('req_title');
            $req_desc                   = $_POST['req_desc'];

            if(!empty($choice_titles) && count($choice_titles) > 0){
                foreach ($choice_titles as $i => $row) {
                    $choice_options         = $this->input->post('op_set'.$choice_no[$i]);
                    $options[]              =   array(
                                                    'no' => $choice_no[$i],
                                                    'title' => $choice_titles[$i],
                                                    'name' => 'choice_'.$choice_no[$i],
                                                    'type' => $choice_types[$i],
                                                    'option' => $choice_options
                                                );
                }
            }

            if(!empty($req_title)){
                foreach($req_title as $i => $row){
                    $requirements[] = array('index'=>$i,'field'=>$row,'desc'=>$req_desc[$i]);
                }
            }

            $data['options'] = json_encode($options);
            // $data['demo'] = $this->input->post('demo');
            $data['recipe'] = $this->input->post('recipe');
            $data['eprice'] = $this->input->post('eprice');
            $data['ecatalog'] = $this->input->post('ecatalog');
            $data['requirements'] = json_encode($requirements);

            $data['high_price']     = $this->input->post('high_price');
            $data['high_price_lock']     = $this->input->post('high_price_lock');
            $data['low_price']     = $this->input->post('low_price');
            $data['low_price_lock']     = $this->input->post('low_price_lock');

            /*if (!empty($_FILES['recipe']['name'])) {
                $recipe = $this->crud_model->upload('recipe', 'uploads/recipe', uniqid());
                $data['recipe'] = $recipe['name'];
                if (file_exists('uploads/recipe' . $data['recipe'])) {
                    unlink('uploads/recipe' . $data['recipe']);
                }
            }*/
            
            $this->db->where('manufacture_product_id', $para2);
            $this->db->update('manufacture_product', $data);
            $data2 = $data;
            
            unset($data2['sale_price'], $data2['suggested_price'], $data2['unit'], $data2['discount'], $data2['discount_type']);
            $ids = $this->db->get_where('product', array('manufacture_product_id' => $para2))->row()->product_id;

            $this->crud_model->file_up("images", "manufacture_product", $para2, 'multi');
            $this->crud_model->copy_img_product($para2, $ids, $data['num_of_imgs']);

            $this->db->where('manufacture_product_id', $para2);
            $this->db->update('product', $data2);
            $this->crud_model->set_category_data(0);
            recache();
        } else if ($para1 == 'edit') {
            $page_data['product_data'] = $this->db->get_where('manufacture_product', array(
                'manufacture_product_id' => $para2
            ))->result_array();
            $this->load->view('back/manufacture/product_edit', $page_data);
        } else if ($para1 == 'view') {
            $page_data['product_data'] = $this->db->get_where('manufacture_product', array(
                'manufacture_product_id' => $para2
            ))->result_array();
            $this->load->view('back/manufacture/product_view', $page_data);
        } elseif ($para1 == 'delete') {
            $product_id = $this->db->get_where('product', array('manufacture_product_id' => $para2))->row()->product_id;
            if($product_id){
                //Delete table product
                $this->crud_model->file_dlt('product', $product_id, '.jpg', 'multi');
                $this->db->where('product_id', $product_id);
                $this->db->delete('product');
            }
            //Delete table manufacture_product
            $this->crud_model->file_dlt('manufacture_product', $para2, '.jpg', 'multi');
            $this->db->where('manufacture_product_id', $para2);
            $this->db->delete('manufacture_product');
            $this->crud_model->set_category_data(0);
            recache();
        } elseif ($para1 == 'list') {
            $this->db->order_by('manufacture_product_id', 'desc');
            $this->db->where('added_by',json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id'))));
            $this->db->where('download=',NULL);
            $page_data['all_product'] = $this->db->get('manufacture_product')->result_array();
            $this->load->view('back/manufacture/product_list', $page_data);
        } elseif ($para1 == 'list_data') {
            $limit      = $this->input->get('limit');
            $search     = $this->input->get('search');
            $order      = $this->input->get('order');
            $offset     = $this->input->get('offset');
            $sort       = $this->input->get('sort');
            if($search){
                $this->db->like('title', $search, 'both');
            }
            $this->db->where('download=',NULL);
            $this->db->where('added_by',json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id'))));
            $total      = $this->db->get('manufacture_product')->num_rows();
            $this->db->limit($limit);
            if($sort == ''){
                $sort = 'manufacture_product_id';
                $order = 'DESC';
            }
            $this->db->order_by($sort,$order);
            if($search){
                $this->db->like('title', $search, 'both');
            }
            $this->db->where('added_by',json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id'))));
            $this->db->where('download=',NULL);
            $products   = $this->db->get('manufacture_product', $limit, $offset)->result_array();
            $data       = array();
            foreach ($products as $row) {

                $res    = array(
                             'image' => '',
                             'title' => '',
                             'current_stock' => '',
                             'publish' => '',
                             'featured' => '',
                             'options' => ''
                          );

                $res['image']  = '<img class="img-sm" style="height:auto !important; border:1px solid #ddd;padding:2px; border-radius:2px !important;" src="'.$this->crud_model->file_view('manufacture_product',$row['manufacture_product_id'],'','','thumb','src','multi','one').'"  />';
                $res['title']  = $row['title'];
                if($row['status'] == 'ok'){
                    $res['publish']  = '<input id="pub_'.$row['manufacture_product_id'].'" class="sw1" type="checkbox" data-id="'.$row['manufacture_product_id'].'" checked />';
                } else {
                    $res['publish']  = '<input id="pub_'.$row['manufacture_product_id'].'" class="sw1" type="checkbox" data-id="'.$row['manufacture_product_id'].'" />';
                }
                if($row['manufacture_featured'] == 'ok'){
                    $res['featured']  = '<input id="v_fet_'.$row['manufacture_product_id'].'" class="sw4" type="checkbox" data-id="'.$row['manufacture_product_id'].'" checked />';
                } else {
                    $res['featured']  = '<input id="v_fet_'.$row['manufacture_product_id'].'" class="sw4" type="checkbox" data-id="'.$row['manufacture_product_id'].'" />';
                }
                if($row['current_stock'] > 0){ 
                    $res['current_stock']  = $row['current_stock'].$row['unit'].'(s)';                     
                } else {
                    $res['current_stock']  = '<span class="label label-danger">'.translate('out_of_stock').'</span>';
                }

                //add html for action
                $res['options'] = "  <a class=\"btn btn-info btn-xs btn-labeled fa fa-location-arrow\" data-toggle=\"tooltip\" 
                                onclick=\"ajax_set_full('view','".translate('view_product')."','".translate('successfully_viewed!')."','product_view','".$row['manufacture_product_id']."');proceed('to_list');\" data-original-title=\"View\" data-container=\"body\">
                                    ".translate('view')."
                            </a>
                            <a class=\"btn btn-purple btn-xs btn-labeled fa fa-tag\" data-toggle=\"tooltip\"
                                onclick=\"ajax_modal('add_discount','".translate('view_discount')."','".translate('viewing_discount!')."','add_discount','".$row['manufacture_product_id']."')\" data-original-title=\"Edit\" data-container=\"body\">
                                    ".translate('discount')."
                            </a>
                            
                            
                            <a class=\"btn btn-success btn-xs btn-labeled fa fa-wrench\" data-toggle=\"tooltip\" 
                                onclick=\"ajax_set_full('edit','".translate('edit_product')."','".translate('successfully_edited!')."','product_edit','".$row['manufacture_product_id']."');proceed('to_list');\" data-original-title=\"Edit\" data-container=\"body\">
                                    ".translate('edit')."
                            </a>
                            
                            <a onclick=\"delete_confirm('".$row['manufacture_product_id']."','".translate('really_want_to_delete_this?')."')\" 
                                class=\"btn btn-danger btn-xs btn-labeled fa fa-trash\" data-toggle=\"tooltip\" data-original-title=\"Delete\" data-container=\"body\">
                                    ".translate('delete')."
                            </a>";
                $data[] = $res;
            }
            $result = array(
                             'total' => $total,
                             'rows' => $data
                           );

            echo json_encode($result);

        } else if ($para1 == 'dlt_img') {
            $a = explode('_', $para2);
            $this->crud_model->file_dlt('manufacture_product', $a[0], '.jpg', 'multi', $a[1]);
            $get_id = $this->db->get_where('product', array('manufacture_product_id' => $a[0]))->result();
            foreach($get_id as $row){
                $ids[] = $row->product_id;
            }
            $this->crud_model->multi_delete_file('product', $ids, '.jpg', 'multi', $a[1]);
            recache();
        } elseif ($para1 == 'sub_by_cat') {
            echo $this->crud_model->select_html('sub_category', 'sub_category', 'sub_category_name', 'add', 'demo-chosen-select required', '', 'category', $para2, 'get_brnd');
        } elseif ($para1 == 'brand_by_sub') {
            $brands=json_decode($this->crud_model->get_type_name_by_id('sub_category',$para2,'brand'),true);
            echo $this->crud_model->select_html('brand', 'brand', 'name', 'add', 'demo-chosen-select required', '', 'brand_id', $brands, '', 'multi');
        } elseif ($para1 == 'product_by_sub') {
            echo $this->crud_model->select_html('product', 'product', 'title', 'add', 'demo-chosen-select required', '', 'sub_category', $para2, 'get_pro_res');
        } elseif ($para1 == 'pur_by_pro') {
            echo $this->crud_model->get_type_name_by_id('product', $para2, 'purchase_price');
        } elseif ($para1 == 'add') {
            if ($this->db->get_where('business_settings',array('type' => 'commission_set'))->row()->value == 'no') {
                if($this->crud_model->manufacture_can_add_product($this->session->userdata('manufacture_id'))){
                    $this->load->view('back/manufacture/product_add');
                } else {
                    $this->load->view('back/manufacture/product_limit');
                }
            }
            elseif($this->db->get_where('business_settings',array('type' => 'commission_set'))->row()->value == 'yes'){
                $this->load->view('back/manufacture/product_add');
            }
        } elseif ($para1 == 'add_stock') {
            $data['product'] = $para2;
            $this->load->view('back/manufacture/product_stock_add', $data);
        } elseif ($para1 == 'destroy_stock') {
            $data['product'] = $para2;
            $this->load->view('back/manufacture/product_stock_destroy', $data);
        } elseif ($para1 == 'stock_report') {
            $data['product'] = $para2;
            $this->load->view('back/manufacture/product_stock_report', $data);
        } elseif ($para1 == 'sale_report') {
            $data['product'] = $para2;
            $this->load->view('back/manufacture/product_sale_report', $data);
        } elseif ($para1 == 'add_discount') {
            $data['product'] = $para2;
            $this->load->view('back/manufacture/product_add_discount', $data);
        } elseif ($para1 == 'product_featured_set') {
            $product = $para2;
            if ($para3 == 'true') {
                $data['featured'] = 'ok';
            } else {
                $data['featured'] = '0';
            }
            $this->db->where('manufacture_product_id', $product);
            $this->db->update('manufacture_product', $data);
            $user = json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id')));
            
            $prod_id = $this->db->get_where('product', array('manufacture_product_id' => $product, 'added_by' => $user))->row()->product_id;
           
            $this->db->where('product_id', $prod_id);
            $this->db->update('product', $data);
            recache();
        } elseif ($para1 == 'product_v_featured_set') {
            $product = $para2;
            if ($para3 == 'true') {
                $data['manufacture_featured'] = 'ok';
            } else {
                $data['manufacture_featured'] = 'no';
            }
            $this->db->where('manufacture_product_id', $product);
            $this->db->update('manufacture_product', $data);
            $user = json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id')));
            $prod_id = $this->db->get_where('product', array('manufacture_product_id' => $product, 'added_by' => $user))->row()->product_id;
            if ($para3 == 'true') {
                $data2['featured'] = 'ok';
            } else {
                $data2['featured'] = 'no';
            }
            $this->db->where('product_id', $prod_id);
            $this->db->update('product', $data2);
            recache();
        } elseif ($para1 == 'product_deal_set') {
            $product = $para2;
            if ($para3 == 'true') {
                $data['deal'] = 'ok';
            } else {
                $data['deal'] = '0';
            }
            $this->db->where('manufacture_product_id', $product);
            $this->db->update('manufacture_product', $data);
            recache();
        } elseif ($para1 == 'product_publish_set') {
            $product = $para2;
            if ($para3 == 'true') {
                $data['status'] = 'ok';
            } else {
                $data['status'] = '0';
            }
            $this->db->where('manufacture_product_id', $product);
            $this->db->update('manufacture_product', $data);
            
            $this->db->where('manufacture_product_id', $product);
            $this->db->update('product', $data);
            
            $this->crud_model->set_category_data(0);
            recache();
        } elseif ($para1 == 'add_discount_set') {
            $product               = $this->input->post('product');
            $data['discount']      = $this->input->post('discount');
            $data['discount_type'] = $this->input->post('discount_type');
            $this->db->where('manufacture_product_id', $product);
            $this->db->update('manufacture_product', $data);
            $this->crud_model->set_category_data(0);
            recache();
        } else {
            $page_data['page_name']   = "product";
            $this->db->where('added_by',json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id'))));
            $page_data['all_product'] = $this->db->get('manufacture_product')->result_array();
            $this->load->view('back/index', $page_data);
        }
    }
    
    /* Digital add, edit, view, delete, stock increase, decrease, discount */
    function digital($para1 = '', $para2 = '', $para3 = '')
    {
        if (!$this->crud_model->manufacture_permission('product')) {
            redirect(base_url() . 'manufacture');
        }
        if ($this->crud_model->get_type_name_by_id('general_settings','69','value') !== 'ok') {
            redirect(base_url() . 'admin');
        }
        if ($para1 == 'do_add') {
            if ($_FILES["images"]['name'][0] == '') {
                $num_of_imgs = 0;
            } else {
                $num_of_imgs = count($_FILES["images"]['name']);
            }
            if ($this->db->get_where('business_settings',array('type' => 'commission_set'))->row()->value == 'no') {
                if($this->crud_model->manufacture_can_add_product($this->session->userdata('manufacture_id'))) {
                    $data['title']              = $this->input->post('title');
                    $data['category']           = $this->input->post('category');
                    $data['description']        = $_POST['description'];
                    $data['sub_category']       = $this->input->post('sub_category');
                    $data['sale_price']         = $this->input->post('sale_price');
                    $data['purchase_price']     = $this->input->post('purchase_price');
                    $data['add_timestamp']      = time();
                    $data['featured']           = 'no';
                    $data['status']             = 'ok';
                    $data['rating_user']        = '[]';
                    $data['tax']                = $this->input->post('tax');
                    $data['discount']           = $this->input->post('discount');
                    $data['discount_type']      = $this->input->post('discount_type');
                    $data['tax_type']           = $this->input->post('tax_type');
                    $data['shipping_cost']      = 0;
                    $data['tag']                = $this->input->post('tag');
                    $data['num_of_imgs']        = $num_of_imgs;
                    $data['front_image']        = $this->input->post('front_image');
                    $additional_fields['name']  = (empty($_POST['ad_field_names'])) ? '' : json_encode($_POST['ad_field_names']);
                    $additional_fields['value'] = (empty($_POST['ad_field_values'])) ? '' : json_encode($_POST['ad_field_values']);
                    $data['additional_fields']  = json_encode($additional_fields);
                    $data['requirements']       =   '[]';
                    $data['video']              =   '[]';
                    
                    $data['added_by']           = json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id')));
                    
                    $this->db->insert('product', $data);
                    $id = $this->db->insert_id();
                    $this->benchmark->mark_time();
                    
                    $this->crud_model->file_up("images", "product", $id, 'multi');
                    
                    $path = $_FILES['logo']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $data_logo['logo']       = 'digital_logo_'.$id.'.'.$ext;
                    $this->db->where('product_id' , $id);
                    $this->db->update('product' , $data_logo);
                    $this->crud_model->file_up("logo", "digital_logo", $id, '','no','.'.$ext);
                    
                    //Requirements add
                    $requirements               =   array();
                    $req_title                  =   $this->input->post('req_title');
                    $req_desc                   =   $this->input->post('req_desc');
                    if(!empty($req_title)){
                        foreach($req_title as $i => $row){
                            $requirements[]         =   array('index'=>$i,'field'=>$row,'desc'=>$req_desc[$i]);
                        }
                    }
                    
                    $data_req['requirements']           =   json_encode($requirements);
                    $this->db->where('product_id' , $id);
                    $this->db->update('product' , $data_req);
                    
                    //File upload
                    $rand           = substr(hash('sha512', rand()), 0, 20);
                    $name           = $id.'_'.$rand.'_'.$_FILES['product_file']['name'];
                    $da['download_name'] = $name;
                    $da['download'] = 'ok';
                    $folder = $this->db->get_where('general_settings', array('type' => 'file_folder'))->row()->value;
                    move_uploaded_file($_FILES['product_file']['tmp_name'], 'uploads/file_products/' . $folder .'/' . $name);
                    $this->db->where('product_id', $id);
                    $this->db->update('product', $da);
                    
                    //vdo upload
                    $video_details              =   array();
                    if($this->input->post('upload_method') == 'upload'){                
                        $video              =   $_FILES['videoFile']['name'];
                        $ext                =   pathinfo($video,PATHINFO_EXTENSION);
                        move_uploaded_file($_FILES['videoFile']['tmp_name'],'uploads/video_digital_product/digital_'.$id.'.'.$ext);
                        $video_src          =   'uploads/video_digital_product/digital_'.$id.'.'.$ext;
                        $video_details[]    =   array('type'=>'upload','from'=>'local','video_link'=>'','video_src'=>$video_src);
                        $data_vdo['video']  =   json_encode($video_details);
                        $this->db->where('product_id',$id);
                        $this->db->update('product',$data_vdo);     
                    }
                    elseif ($this->input->post('upload_method') == 'share'){
                        $from               = $this->input->post('site');
                        $video_link         = $this->input->post('video_link');
                        $code               = $this->input->post('video_code');
                        if($from=='youtube'){
                            $video_src      = 'https://www.youtube.com/embed/'.$code;
                        }else if($from=='dailymotion'){
                            $video_src      = '//www.dailymotion.com/embed/video/'.$code;
                        }else if($from=='vimeo'){
                            $video_src      = 'https://player.vimeo.com/video/'.$code;
                        }
                        $video_details[]    =   array('type'=>'share','from'=>$from,'video_link'=>$video_link,'video_src'=>$video_src);
                        $data_vdo['video']  =   json_encode($video_details);
                        $this->db->where('product_id',$id);
                        $this->db->update('product',$data_vdo); 
                    }
                } else {
                    echo 'already uploaded maximum product';
                }
            }
            elseif($this->db->get_where('business_settings',array('type' => 'commission_set'))->row()->value == 'yes'){
                $data['title']              = $this->input->post('title');
                $data['category']           = $this->input->post('category');
                $data['description']        = $_POST['description'];
                $data['sub_category']       = $this->input->post('sub_category');
                $data['sale_price']         = $this->input->post('sale_price');
                $data['purchase_price']     = $this->input->post('purchase_price');
                $data['add_timestamp']      = time();
                $data['featured']           = 'no';
                $data['status']             = 'ok';
                $data['rating_user']        = '[]';
                $data['tax']                = $this->input->post('tax');
                $data['discount']           = $this->input->post('discount');
                $data['discount_type']      = $this->input->post('discount_type');
                $data['tax_type']           = $this->input->post('tax_type');
                $data['shipping_cost']      = 0;
                $data['tag']                = $this->input->post('tag');
                $data['num_of_imgs']        = $num_of_imgs;
                $data['front_image']        = $this->input->post('front_image');
                $additional_fields['name']  = (empty($_POST['ad_field_names'])) ? '' : json_encode($_POST['ad_field_names']);
                $additional_fields['value'] = (empty($_POST['ad_field_values'])) ? '' : json_encode($_POST['ad_field_values']);
                $data['additional_fields']  = json_encode($additional_fields);
                $data['requirements']       =   '[]';
                $data['video']              =   '[]';
                
                $data['added_by']           = json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id')));
                
                $this->db->insert('product', $data);
                $id = $this->db->insert_id();
                $this->benchmark->mark_time();
                
                $this->crud_model->file_up("images", "product", $id, 'multi');
                
                $path = $_FILES['logo']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $data_logo['logo']       = 'digital_logo_'.$id.'.'.$ext;
                $this->db->where('product_id' , $id);
                $this->db->update('product' , $data_logo);
                $this->crud_model->file_up("logo", "digital_logo", $id, '','no','.'.$ext);
                
                //Requirements add
                $requirements               =   array();
                $req_title                  =   $this->input->post('req_title');
                $req_desc                   =   $this->input->post('req_desc');
                if(!empty($req_title)){
                    foreach($req_title as $i => $row){
                        $requirements[]         =   array('index'=>$i,'field'=>$row,'desc'=>$req_desc[$i]);
                    }
                }
                
                $data_req['requirements']           =   json_encode($requirements);
                $this->db->where('product_id' , $id);
                $this->db->update('product' , $data_req);
                
                //File upload
                $rand           = substr(hash('sha512', rand()), 0, 20);
                $name           = $id.'_'.$rand.'_'.$_FILES['product_file']['name'];
                $da['download_name'] = $name;
                $da['download'] = 'ok';
                $folder = $this->db->get_where('general_settings', array('type' => 'file_folder'))->row()->value;
                move_uploaded_file($_FILES['product_file']['tmp_name'], 'uploads/file_products/' . $folder .'/' . $name);
                $this->db->where('product_id', $id);
                $this->db->update('product', $da);
                
                //vdo upload
                $video_details              =   array();
                if($this->input->post('upload_method') == 'upload'){                
                    $video              =   $_FILES['videoFile']['name'];
                    $ext                =   pathinfo($video,PATHINFO_EXTENSION);
                    move_uploaded_file($_FILES['videoFile']['tmp_name'],'uploads/video_digital_product/digital_'.$id.'.'.$ext);
                    $video_src          =   'uploads/video_digital_product/digital_'.$id.'.'.$ext;
                    $video_details[]    =   array('type'=>'upload','from'=>'local','video_link'=>'','video_src'=>$video_src);
                    $data_vdo['video']  =   json_encode($video_details);
                    $this->db->where('product_id',$id);
                    $this->db->update('product',$data_vdo);     
                }
                elseif ($this->input->post('upload_method') == 'share'){
                    $from               = $this->input->post('site');
                    $video_link         = $this->input->post('video_link');
                    $code               = $this->input->post('video_code');
                    if($from=='youtube'){
                        $video_src      = 'https://www.youtube.com/embed/'.$code;
                    }else if($from=='dailymotion'){
                        $video_src      = '//www.dailymotion.com/embed/video/'.$code;
                    }else if($from=='vimeo'){
                        $video_src      = 'https://player.vimeo.com/video/'.$code;
                    }
                    $video_details[]    =   array('type'=>'share','from'=>$from,'video_link'=>$video_link,'video_src'=>$video_src);
                    $data_vdo['video']  =   json_encode($video_details);
                    $this->db->where('product_id',$id);
                    $this->db->update('product',$data_vdo); 
                }
            }
            $this->crud_model->set_category_data(0);
            recache();
        } else if ($para1 == "update") {
            $options = array();
            if ($_FILES["images"]['name'][0] == '') {
                $num_of_imgs = 0;
            } else {
                $num_of_imgs = count($_FILES["images"]['name']);
            }
            $num                        = $this->crud_model->get_type_name_by_id('product', $para2, 'num_of_imgs');
            $download                   = $this->crud_model->get_type_name_by_id('product', $para2, 'download');
            $data['title']              = $this->input->post('title');
            $data['category']           = $this->input->post('category');
            $data['description']        = $_POST['description'];
            $data['sub_category']       = $this->input->post('sub_category');
            $data['sale_price']         = $this->input->post('sale_price');
            $data['purchase_price']     = $this->input->post('purchase_price');
            $data['tax']                = $this->input->post('tax');
            $data['discount']           = $this->input->post('discount');
            $data['discount_type']      = $this->input->post('discount_type');
            $data['tax_type']           = $this->input->post('tax_type');
            $data['tag']                = $this->input->post('tag');
            $data['update_time']        = time();
            $data['num_of_imgs']        = $num + $num_of_imgs;
            $data['front_image']        = $this->input->post('front_image');
            $additional_fields['name']  = (empty($_POST['ad_field_names'])) ? '' : json_encode($_POST['ad_field_names']);
            $additional_fields['value'] = (empty($_POST['ad_field_values'])) ? '' : json_encode($_POST['ad_field_values']);
            $data['additional_fields']  = json_encode($additional_fields);
            
            //File upload
            $this->crud_model->file_up("images", "product", $para2, 'multi');
            if($_FILES['product_file']['name'] !== ''){
                $rand           = substr(hash('sha512', rand()), 0, 20);
                $name           = $para2.'_'.$rand.'_'.$_FILES['product_file']['name'];
                $data['download_name'] = $name;
                $folder = $this->db->get_where('general_settings', array('type' => 'file_folder'))->row()->value;
                move_uploaded_file($_FILES['product_file']['tmp_name'], 'uploads/file_products/' . $folder .'/' . $name);
            }
            
            $this->db->where('product_id', $para2);
            $this->db->update('product', $data);
            
            if($_FILES['logo']['name'] !== ''){
                $path = $_FILES['logo']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $data_logo['logo']       = 'digital_logo_'.$para2.'.'.$ext;
                $this->db->where('product_id' , $para2);
                $this->db->update('product' , $data_logo);
                $this->crud_model->file_up("logo", "digital_logo", $para2, '','no','.'.$ext);
            }
            
            //Requirements add
            $requirements               =   array();
            $req_title                  =   $this->input->post('req_title');
            $req_desc                   =   $this->input->post('req_desc');
            if(!empty($req_title)){
                foreach($req_title as $i => $row){
                    $requirements[]         =   array('index'=>$i,'field'=>$row,'desc'=>$req_desc[$i]);
                }
            }
            $data_req['requirements']           =   json_encode($requirements);
            $this->db->where('product_id' , $para2);
            $this->db->update('product' , $data_req);
            
            //vdo upload
            $video_details              =   array();
            if($this->input->post('upload_method') == 'upload'){                
                $video              =   $_FILES['videoFile']['name'];
                $ext                =   pathinfo($video,PATHINFO_EXTENSION);
                move_uploaded_file($_FILES['videoFile']['tmp_name'],'uploads/video_digital_product/digital_'.$para2.'.'.$ext);
                $video_src          =   'uploads/video_digital_product/digital_'.$para2.'.'.$ext;
                $video_details[]    =   array('type'=>'upload','from'=>'local','video_link'=>'','video_src'=>$video_src);
                $data_vdo['video']  =   json_encode($video_details);
                $this->db->where('product_id',$para2);
                $this->db->update('product',$data_vdo);     
            }
            elseif ($this->input->post('upload_method') == 'share'){
                $video= json_decode($this->crud_model->get_type_name_by_id('product',$para2,'video'),true);
                if($video[0]['type'] == 'upload'){
                    if(file_exists($video[0]['video_src'])){
                        unlink($video[0]['video_src']);         
                    }
                }
                $from               = $this->input->post('site');
                $video_link         = $this->input->post('video_link');
                $code               = $this->input->post('video_code');
                if($from=='youtube'){
                    $video_src      = 'https://www.youtube.com/embed/'.$code;
                }else if($from=='dailymotion'){
                    $video_src      = '//www.dailymotion.com/embed/video/'.$code;
                }else if($from=='vimeo'){
                    $video_src      = 'https://player.vimeo.com/video/'.$code;
                }
                $video_details[]    =   array('type'=>'share','from'=>$from,'video_link'=>$video_link,'video_src'=>$video_src);
                $data_vdo['video']  =   json_encode($video_details);
                $this->db->where('product_id',$para2);
                $this->db->update('product',$data_vdo); 
            }
            elseif ($this->input->post('upload_method') == 'delete'){
                $data_vdo['video']  =   '[]';
                $this->db->where('product_id',$para2);
                $this->db->update('product',$data_vdo);
                
                $video= json_decode($this->crud_model->get_type_name_by_id('product',$para2,'video'),true);
                if($video[0]['type'] == 'upload'){
                    if(file_exists($video[0]['video_src'])){
                        unlink($video[0]['video_src']);         
                    }
                }
            }
            $this->crud_model->set_category_data(0);
            
            recache();
        } else if ($para1 == 'edit') {
            $page_data['product_data'] = $this->db->get_where('product', array(
                'product_id' => $para2
            ))->result_array();
            $this->load->view('back/manufacture/digital_edit', $page_data);
        } else if ($para1 == 'view') {
            $page_data['product_data'] = $this->db->get_where('product', array(
                'product' => $para2
            ))->result_array();
            $this->load->view('back/manufacture/digital_view', $page_data);
        } else if ($para1 == 'download_file') {
            $this->crud_model->download_product($para2);
        } else if ($para1 == 'can_download') {
            if($this->crud_model->can_download($para2)){
                echo "yes";
            } else{
                echo "no";
            }
        } elseif ($para1 == 'delete') {
            $this->crud_model->file_dlt('product', $para2, '.jpg', 'multi');
            unlink("uploads/digital_logo_image/" .$this->crud_model->get_type_name_by_id('product',$para2,'logo'));
            $video=$this->crud_model->get_type_name_by_id('product',$para2,'video');
            if($video!=='[]'){
                $video_details= json_decode($video,true);
                if($video_details[0]['type'] == 'upload'){
                    if(file_exists($video_details[0]['video_src'])){
                        unlink($video_details[0]['video_src']);         
                    }
                }
            }
            $this->db->where('product_id', $para2);
            $this->db->delete('product');
            $this->crud_model->set_category_data(0);
            recache();
        } elseif ($para1 == 'list') {
            $this->db->order_by('product_id', 'desc');
            $this->db->where('added_by',json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id'))));
            $this->db->where('download=','ok');
            $page_data['all_product'] = $this->db->get('product')->result_array();
            $this->load->view('back/manufacture/digital_list', $page_data);
        } elseif ($para1 == 'list_data') {
            $limit      = $this->input->get('limit');
            $search     = $this->input->get('search');
            $order      = $this->input->get('order');
            $offset     = $this->input->get('offset');
            $sort       = $this->input->get('sort');
            if($search){
                $this->db->like('title', $search, 'both');
            }
            $this->db->where('added_by',json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id'))));
            $this->db->where('download=','ok');
            $total= $this->db->get('product')->num_rows();
            $this->db->limit($limit);
            if($sort == ''){
                $sort = 'product_id';
                $order = 'DESC';
            }
            $this->db->order_by($sort,$order);
            if($search){
                $this->db->like('title', $search, 'both');
            }
            $this->db->where('added_by',json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id'))));
            $this->db->where('download=','ok');
            $products   = $this->db->get('product', $limit, $offset)->result_array();
            $data       = array();
            foreach ($products as $row) {

                $res    = array(
                             'image' => '',
                             'title' => '',
                             'publish' => '',
                             'options' => ''
                          );

                $res['image']  = '<img class="img-sm" style="height:auto !important; border:1px solid #ddd;padding:2px; border-radius:2px !important;" src="'.$this->crud_model->file_view('product',$row['product_id'],'','','thumb','src','multi','one').'"  />';
                $res['title']  = $row['title'];
                if($row['status'] == 'ok'){
                    $res['publish']  = '<input id="pub_'.$row['product_id'].'" class="sw1" type="checkbox" data-id="'.$row['product_id'].'" checked />';
                } else {
                    $res['publish']  = '<input id="pub_'.$row['product_id'].'" class="sw1" type="checkbox" data-id="'.$row['product_id'].'" />';
                }

                //add html for action
                $res['options'] = "  <a class=\"btn btn-info btn-xs btn-labeled fa fa-location-arrow\" data-toggle=\"tooltip\" 
                                onclick=\"ajax_set_full('view','".translate('view_product')."','".translate('successfully_viewed!')."','digital_view','".$row['product_id']."');proceed('to_list');\" data-original-title=\"View\" data-container=\"body\">
                                    ".translate('view')."
                            </a>
                            <a class=\"btn btn-purple btn-xs btn-labeled fa fa-tag\" data-toggle=\"tooltip\"
                                onclick=\"ajax_modal('add_discount','".translate('view_discount')."','".translate('viewing_discount!')."','add_discount','".$row['product_id']."')\" data-original-title=\"Edit\" data-container=\"body\">
                                    ".translate('discount')."
                            </a>
                            <a class=\"btn btn-mint btn-xs btn-labeled fa fa-download\" data-toggle=\"tooltip\" 
                                onclick=\"digital_download(".$row['product_id'].")\" data-original-title=\"Download\" data-container=\"body\">
                                    ".translate('download')."
                            </a>
                            
                            <a class=\"btn btn-success btn-xs btn-labeled fa fa-wrench\" data-toggle=\"tooltip\" 
                                onclick=\"ajax_set_full('edit','".translate('edit_product_(_digital_product_)')."','".translate('successfully_edited!')."','digital_edit','".$row['product_id']."');proceed('to_list');\" data-original-title=\"Edit\" data-container=\"body\">
                                    ".translate('edit')."
                            </a>
                            
                            <a onclick=\"delete_confirm('".$row['product_id']."','".translate('really_want_to_delete_this?')."')\" 
                                class=\"btn btn-danger btn-xs btn-labeled fa fa-trash\" data-toggle=\"tooltip\" data-original-title=\"Delete\" data-container=\"body\">
                                    ".translate('delete')."
                            </a>";
                $data[] = $res;
            }
            $result = array(
                             'total' => $total,
                             'rows' => $data
                           );

            echo json_encode($result);

        } else if ($para1 == 'dlt_img') {
            $a = explode('_', $para2);
            $this->crud_model->file_dlt('product', $a[0], '.jpg', 'multi', $a[1]);
            recache();
        } elseif ($para1 == 'sub_by_cat') {
            echo $this->crud_model->select_html('sub_category', 'sub_category', 'sub_category_name', 'add', 'demo-chosen-select required', '', 'category', $para2, '');
        } elseif ($para1 == 'product_by_sub') {
            echo $this->crud_model->select_html('product', 'product', 'title', 'add', 'demo-chosen-select required', '', 'sub_category', $para2, 'get_pro_res');
        } 
        elseif ($para1 == 'pur_by_pro') {
            echo $this->crud_model->get_type_name_by_id('product', $para2, 'purchase_price');
        }elseif ($para1 == 'add') {
            if ($this->db->get_where('business_settings',array('type' => 'commission_set'))->row()->value == 'no') {
                if($this->crud_model->manufacture_can_add_product($this->session->userdata('manufacture_id'))){
                    $this->load->view('back/manufacture/digital_add');
                } else {
                    $this->load->view('back/manufacture/product_limit');
                }
            }
            elseif ($this->db->get_where('business_settings',array('type' => 'commission_set'))->row()->value == 'yes') {
                $this->load->view('back/manufacture/digital_add');
            }
            //$this->load->view('back/manufacture/digital_add');
        } elseif ($para1 == 'sale_report') {
            $data['product'] = $para2;
            $this->load->view('back/manufacture/product_sale_report', $data);
        } elseif ($para1 == 'add_discount') {
            $data['product'] = $para2;
            $this->load->view('back/manufacture/digital_add_discount', $data);
        } elseif ($para1 == 'product_featured_set') {
            $product = $para2;
            if ($para3 == 'true') {
                $data['featured'] = 'ok';
            } else {
                $data['featured'] = '0';
            }
            $this->db->where('product_id', $product);
            $this->db->update('product', $data);
            recache();
        } elseif ($para1 == 'product_deal_set') {
            $product = $para2;
            if ($para3 == 'true') {
                $data['deal'] = 'ok';
            } else {
                $data['deal'] = '0';
            }
            $this->db->where('product_id', $product);
            $this->db->update('product', $data);
            recache();
        } elseif ($para1 == 'product_publish_set') {
            $product = $para2;
            if ($para3 == 'true') {
                $data['status'] = 'ok';
            } else {
                $data['status'] = '0';
            }
            $this->db->where('product_id', $product);
            $this->db->update('product', $data);
            $this->crud_model->set_category_data(0);
            recache();
        } elseif ($para1 == 'add_discount_set') {
            $product               = $this->input->post('product');
            $data['discount']      = $this->input->post('discount');
            $data['discount_type'] = $this->input->post('discount_type');
            $this->db->where('product_id', $product);
            $this->db->update('product', $data);
            $this->crud_model->set_category_data(0);
            recache();
        }elseif ($para1 == 'video_preview') {
            if($para2 == 'youtube'){
                echo '<iframe width="400" height="300" src="https://www.youtube.com/embed/'.$para3.'" frameborder="0"></iframe>';
            }else if($para2 == 'dailymotion'){
                echo '<iframe width="400" height="300" src="//www.dailymotion.com/embed/video/'.$para3.'" frameborder="0"></iframe>';
            }else if($para2 == 'vimeo'){
                echo '<iframe src="https://player.vimeo.com/video/'.$para3.'" width="400" height="300" frameborder="0"></iframe>';
            }
        }else {
            $page_data['page_name']   = "digital";
            $this->db->order_by('product_id', 'desc');
            $this->db->where('added_by',json_encode(array('type'=>'manufacture','id'=>$this->session->userdata('manufacture_id'))));
            $this->db->where('download=','ok');
            $page_data['all_product'] = $this->db->get('product')->result_array();
            $this->load->view('back/index', $page_data);
        }
    }
    
    function admin_payments($para1='', $para2=''){
        if(!$this->crud_model->manufacture_permission('pay_to_manufacture')){
            redirect(base_url() . 'manufacture');
        }
        if($para1 == 'list'){
            $this->db->order_by('manufacture_invoice_id','desc');
            $page_data['payment_list']  = $this->db->get_where('manufacture_invoice',array('manufacture_id' => $this->session->userdata('manufacture_id')))->result_array();
            $this->load->view('back/manufacture/admin_payments_list',$page_data);
        }
        else if($para1 == 'view'){
            $page_data['details']  = $this->db->get_where('manufacture_invoice',array('manufacture_id' => $this->session->userdata('manufacture_id'), 'manufacture_invoice_id' => $para2))->result_array();
            $this->load->view('back/manufacture/admin_payments_view',$page_data);
        }
        else{
            $page_data['page_name'] = 'admin_payments';
            $this->load->view('back/index',$page_data);
        }
        
    }
    
    /* Package Upgrade History */ 
    
    function upgrade_history($para1='',$para2=''){
        if(!$this->crud_model->manufacture_permission('business_settings')){
            redirect(base_url() . 'manufacture');
        }
        if($para1=='list'){
            $this->db->order_by('membership_payment_id','desc');
            $page_data['package_history']   = $this->db->get_where('manufacture_membership_payment',array('manufacture' => $this->session->userdata('manufacture_id')))->result_array();
            $this->load->view('back/manufacture/upgrade_history_list',$page_data);
        }
        else if($para1 == 'view'){
            $page_data['upgrade_history_data'] = $this->db->get_where('manufacture_membership_payment',array('membership_payment_id' => $para2))->result_array();
            $this->load->view('back/manufacture/upgrade_history_view',$page_data);
        }
        else{
            $page_data['page_name'] = 'upgrade_history';
            $this->load->view('back/index',$page_data);
        }
    }
    
    /* Checking Login Stat */
    function is_logged()
    {
        if ($this->session->userdata('manufacture_login') == 'yes') {
            echo 'yah!good';
        } else {
            echo 'nope!bad';
        }
    }
    

    /* Manage Business Settings */
    function package($para1 = "", $para2 = "")
    {
        if ($para1 == 'upgrade') {
            $method         = $this->input->post('method');
            $type           = $this->input->post('membership');
            $manufacture         = $this->session->userdata('manufacture_id');
            if($type !== '0'){
                $amount         = $this->db->get_where('manufacture_membership',array('membership_id'=>$type))->row()->price;
                $amount_in_usd  = $amount/exchange('usd');
                //echo exchange('usd');exit;

                if ($method == 'paypal') {

                    $paypal_email           = $this->db->get_where('business_settings',array('type'=>'paypal_email'))->row()->value;
                    $data['manufacture']         = $manufacture;
                    $data['amount']         = $amount;
                    $data['status']         = 'due';
                    $data['method']         = 'paypal';
                    $data['membership']     = $type; 
                    $data['timestamp']      = time();

                    $this->db->insert('manufacture_membership_payment', $data);
                    $invoice_id           = $this->db->insert_id();
                    $this->session->set_userdata('invoice_id', $invoice_id);
                    
                    /****TRANSFERRING USER TO PAYPAL TERMINAL****/
                    $this->paypal->add_field('rm', 2);
                    $this->paypal->add_field('no_note', 0);
                    $this->paypal->add_field('cmd', '_xclick');
                    
                    $this->paypal->add_field('amount', $this->cart->format_number($amount_in_usd));

                    //$this->paypal->add_field('amount', $grand_total);
                    $this->paypal->add_field('custom', $invoice_id);
                    $this->paypal->add_field('business', $paypal_email);
                    $this->paypal->add_field('notify_url', base_url() . 'manufacture/paypal_ipn');
                    $this->paypal->add_field('cancel_return', base_url() . 'manufacture/paypal_cancel');
                    $this->paypal->add_field('return', base_url() . 'manufacture/paypal_success');
                    
                    $this->paypal->submit_paypal_post();
                    // submit the fields to paypal

                }elseif ($method == 'pum') {

                    $pum_key           = $this->db->get_where('business_settings',array('type'=>'pum_merchant_key'))->row()->value;
                    $pum_salt           = $this->db->get_where('business_settings',array('type'=>'pum_merchant_salt'))->row()->value;
                    $data['manufacture']         = $manufacture;
                    $data['amount']         = $amount;
                    $data['status']         = 'due';
                    $data['method']         = 'PayUmoney';
                    $data['membership']     = $type; 
                    $data['timestamp']      = time();

                    $this->db->insert('manufacture_membership_payment', $data);
                    $invoice_id           = $this->db->insert_id();
                    $this->session->set_userdata('invoice_id', $invoice_id);
                    
                    $this->pum->add_field('key', $pum_key);
                    $this->pum->add_field('txnid',substr(hash('sha256', mt_rand() . microtime()), 0, 20));
                    $this->pum->add_field('amount', $amount);
                    $this->pum->add_field('firstname', $this->db->get_where('manufacture', array('manufacture_id' => $manufacture))->row()->name);
                    $this->pum->add_field('email', $this->db->get_where('manufacture', array('manufacture_id' => $manufacture))->row()->email);
                    $this->pum->add_field('phone', 'Not Given');
                    $this->pum->add_field('productinfo', 'Payment with PayUmoney');
                    $this->pum->add_field('service_provider', 'payu_paisa');
                    $this->pum->add_field('udf1', $manufacture);
                    
                    $this->pum->add_field('surl', base_url().'manufacture/manufacture_pum_success');
                    $this->pum->add_field('furl', base_url().'manufacture/manufacture_pum_failure');
                    
                    // submit the fields to pum
                    $this->pum->submit_pum_post();

                }elseif ($method == 'ssl') {

                    $data['manufacture']         = $manufacture;
                    $data['amount']         = $amount;
                    $data['status']         = 'due';
                    $data['method']         = 'SSlcommerz';
                    $data['membership']     = $type; 
                    $data['timestamp']      = time();

                    $this->db->insert('manufacture_membership_payment', $data);
                    $invoice_id           = $this->db->insert_id();
                    $this->session->set_userdata('invoice_id', $invoice_id);
                    
                    $ssl_store_id = $this->db->get_where('business_settings', array('type' => 'ssl_store_id'))->row()->value;
                    $ssl_store_passwd = $this->db->get_where('business_settings', array('type' => 'ssl_store_passwd'))->row()->value;
                    $ssl_type = $this->db->get_where('business_settings', array('type' => 'ssl_type'))->row()->value;

                    /* PHP */
                    $post_data = array();
                    $post_data['store_id'] = $ssl_store_id;
                    $post_data['store_passwd'] = $ssl_store_passwd;
                    $post_data['total_amount'] = $amount;
                    $post_data['currency'] = "BDT";
                    $post_data['tran_id'] = date('Ym', $data['timestamp']) . $invoice_id;
                    $post_data['success_url'] = base_url()."manufacture/manufacture_sslcommerz_success";
                    $post_data['fail_url'] = base_url()."manufacture/manufacture_sslcommerz_fail";
                    $post_data['cancel_url'] = base_url()."manufacture/manufacture_sslcommerz_cancel";
                    # $post_data['multi_card_name'] = "mastercard,visacard,amexcard";  # DISABLE TO DISPLAY ALL AVAILABLE

                    # EMI INFO
                    $post_data['emi_option'] = "1";
                    $post_data['emi_max_inst_option'] = "9";
                    $post_data['emi_selected_inst'] = "9";

                    $user_id = $this->session->userdata('manufacture_id');
                    $user_info = $this->db->get_where('manufacture', array('manufacture_id' => $user_id))->row();

                    $cus_name = $user_info->name;
                    
                    # CUSTOMER INFORMATION
                    $post_data['cus_name'] = $cus_name;
                    $post_data['cus_email'] = $user_info->email;
                    $post_data['cus_add1'] = $user_info->address1;
                    $post_data['cus_add2'] = $user_info->address2;
                    $post_data['cus_city'] = $user_info->city;
                    $post_data['cus_state'] = $user_info->state;
                    $post_data['cus_postcode'] = $user_info->zip;
                    $post_data['cus_country'] = $user_info->country;
                    $post_data['cus_phone'] = $user_info->phone;

                    # REQUEST SEND TO SSLCOMMERZ
                    if ($ssl_type == "sandbox") {
                        $direct_api_url = "https://sandbox.sslcommerz.com/gwprocess/v3/api.php"; // Sandbox
                    } elseif ($ssl_type == "live") {
                        $direct_api_url = "https://securepay.sslcommerz.com/gwprocess/v3/api.php"; // Live
                    }

                    $handle = curl_init();
                    curl_setopt($handle, CURLOPT_URL, $direct_api_url );
                    curl_setopt($handle, CURLOPT_TIMEOUT, 30);
                    curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 30);
                    curl_setopt($handle, CURLOPT_POST, 1 );
                    curl_setopt($handle, CURLOPT_POSTFIELDS, $post_data);
                    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
                    if ($ssl_type == "sandbox") {
                        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE); # KEEP IT FALSE IF YOU RUN FROM LOCAL PC
                    } elseif ($ssl_type == "live") {
                        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, TRUE);
                    }


                    $content = curl_exec($handle);

                    $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

                    if($code == 200 && !( curl_errno($handle))) {
                        curl_close( $handle);
                        $sslcommerzResponse = $content;
                    } else {
                        curl_close( $handle);
                        echo "FAILED TO CONNECT WITH SSLCOMMERZ API";
                        exit;
                    }

                    # PARSE THE JSON RESPONSE
                    $sslcz = json_decode($sslcommerzResponse, true );

                    if(isset($sslcz['GatewayPageURL']) && $sslcz['GatewayPageURL']!="" ) {
                        # THERE ARE MANY WAYS TO REDIRECT - Javascript, Meta Tag or Php Header Redirect or Other
                        # echo "<script>window.location.href = '". $sslcz['GatewayPageURL'] ."';</script>";
                        echo "<meta http-equiv='refresh' content='0;url=".$sslcz['GatewayPageURL']."'>";
                        # header("Location: ". $sslcz['GatewayPageURL']);
                        exit;
                    } else {
                        echo "JSON Data parsing error!";
                    }

                }else if ($method == 'c2') {
                    $data['manufacture']         = $manufacture;
                    $data['amount']         = $amount;
                    $data['status']         = 'due';
                    $data['method']         = 'c2';
                    $data['membership']     = $type; 
                    $data['timestamp']      = time();

                    $this->db->insert('manufacture_membership_payment', $data);
                    $invoice_id           = $this->db->insert_id();
                    $this->session->set_userdata('invoice_id', $invoice_id);

                    $c2_user = $this->db->get_where('business_settings',array('type' => 'c2_user'))->row()->value; 
                    $c2_secret = $this->db->get_where('business_settings',array('type' => 'c2_secret'))->row()->value;
                    

                    $this->twocheckout_lib->set_acct_info($c2_user, $c2_secret, 'Y');
                    $this->twocheckout_lib->add_field('sid', $this->twocheckout_lib->sid);              //Required - 2Checkout account number
                    $this->twocheckout_lib->add_field('cart_order_id', $invoice_id);   //Required - Cart ID
                    $this->twocheckout_lib->add_field('total',$this->cart->format_number($amount_in_usd));          
                    
                    $this->twocheckout_lib->add_field('x_receipt_link_url', base_url().'manufacture/twocheckout_success');
                    $this->twocheckout_lib->add_field('demo', $this->twocheckout_lib->demo);                    //Either Y or N
                    
                    $this->twocheckout_lib->submit_form();
                }else if($method == 'vp'){
                    $vp_id                  = $this->db->get_where('business_settings',array('type'=>'vp_merchant_id'))->row()->value;
                    $data['manufacture']         = $manufacture;
                    $data['amount']         = $amount;
                    $data['status']         = 'due';
                    $data['method']         = 'vouguepay';
                    $data['membership']     = $type; 
                    $data['timestamp']      = time();

                    $this->db->insert('manufacture_membership_payment', $data);
                    $invoice_id           = $this->db->insert_id();
                    $this->session->set_userdata('invoice_id', $invoice_id);

                    /****TRANSFERRING USER TO vouguepay TERMINAL****/
                    $this->vouguepay->add_field('v_merchant_id', $vp_id);
                    $this->vouguepay->add_field('merchant_ref', $invoice_id);
                    $this->vouguepay->add_field('memo', 'Package Upgrade to '.$type);
                    //$this->vouguepay->add_field('developer_code', $developer_code);
                    //$this->vouguepay->add_field('store_id', $store_id);

                    
                    $this->vouguepay->add_field('total', $amount);

                    //$this->vouguepay->add_field('amount', $grand_total);
                    //$this->vouguepay->add_field('custom', $sale_id);
                    //$this->vouguepay->add_field('business', $vouguepay_email);

                    $this->vouguepay->add_field('notify_url', base_url() . 'manufacture/vouguepay_ipn');
                    $this->vouguepay->add_field('fail_url', base_url() . 'manufacture/vouguepay_cancel');
                    $this->vouguepay->add_field('success_url', base_url() . 'manufacture/vouguepay_success');
                    
                    $this->vouguepay->submit_vouguepay_post();
                    // submit the fields to vouguepay
                } else if ($method == 'stripe') {
                    if($this->input->post('stripeToken')) {
                        
                        $stripe_api_key = $this->db->get_where('business_settings' , array('type' => 'stripe_secret'))->row()->value;
                        require_once(APPPATH . 'libraries/stripe-php/init.php');
                        \Stripe\Stripe::setApiKey($stripe_api_key); //system payment settings
                        $manufacture_email = $this->db->get_where('manufacture' , array('manufacture_id' => $manufacture))->row()->email;
                        
                        $manufacturea = \Stripe\Customer::create(array(
                            'email' => $manufacture_email, // customer email id
                            'card'  => $_POST['stripeToken']
                        ));

                        $charge = \Stripe\Charge::create(array(
                            'customer'  => $manufacturea->id,
                            'amount'    => ceil($amount_in_usd*100),
                            'currency'  => 'USD'
                        ));

                        if($charge->paid == true){
                            $manufacturea = (array) $manufacturea;
                            $charge = (array) $charge;
                            
                            $data['manufacture']         = $manufacture;
                            $data['amount']         = $amount;
                            $data['status']         = 'paid';
                            $data['method']         = 'stripe';
                            $data['timestamp']      = time();
                            $data['membership']     = $type;
                            $data['details']        = "Customer Info: \n".json_encode($manufacturea,true)."\n \n Charge Info: \n".json_encode($charge,true);
                            
                            $this->db->insert('manufacture_membership_payment', $data);
                            $this->crud_model->upgrade_membership($manufacture,$type);
                            redirect(base_url() . 'manufacture/package/', 'refresh');
                        } else {
                            $this->session->set_flashdata('alert', 'unsuccessful_stripe');
                            redirect(base_url() . 'manufacture/package/', 'refresh');
                        }
                        
                    } else{
                        $this->session->set_flashdata('alert', 'unsuccessful_stripe');
                        redirect(base_url() . 'manufacture/package/', 'refresh');
                    }
                } else if(explode("_",$method)[0] == 'duitku'){
                    $data['manufacture']         = $manufacture;
                    $data['amount']         = $amount;
                    $data['status']         = 'due';
                    $data['method']         = 'duitku';
                    $data['membership']     = $type; 
                    $data['timestamp']      = time();

                    $this->db->insert('manufacture_membership_payment', $data);
                    $invoice_id           = $this->db->insert_id();
                    $this->session->set_userdata('invoice_id', $invoice_id);
                    $duitku_merchant_code = $this->db->get_where('business_settings', array('type' => 'duitku_code'))->row()->value;
                    $duitku_merchant_key = $this->db->get_where('business_settings', array('type' => 'duitku_key'))->row()->value;
                    $duitku_type = $this->db->get_where('business_settings', array('type' => 'duitku_type'))->row()->value;
                    $get_cc = explode("_",$method)[1];
                    if($get_cc == 'cc'){
                        $pm = 'VC';
                    } else if($get_cc == 'bca'){
                        $pm = 'BK';
                    } else if($get_cc == 'mandiri'){
                        $pm = 'M1';
                    } else if($get_cc == 'permata'){
                        $pm = 'BT';
                    } else if($get_cc == 'cimb'){
                        $pm = 'B1';
                    } else if($get_cc == 'trans'){
                        $pm = 'A1';
                    } else if($get_cc == 'bni'){
                        $pm = 'I1';
                    } else if($get_cc == 'danamon'){
                        $pm = 'I2';
                    } else if($get_cc == 'may'){
                        $pm = 'VA';
                    } else if($get_cc == 'ritel'){
                        $pm = 'FT';
                    } else if($get_cc == 'ovo'){
                        $pm = 'OV';
                    }

                    $user_id = $this->session->userdata('manufacture_id');
                    $user_info = $this->db->get_where('manufacture', array('manufacture_id' => $user_id))->row();
                    $cus_name = $user_info->name;

                    $unix = 'manufacturepackagepayment_'.$invoice_id;
                    if($pm !== ''){
                        $post_data = array();
                        $post_data['merchantCode'] = $duitku_merchant_code;
                        $post_data['paymentAmount'] = $amount;
                        $post_data['productDetails'] = 'upgrade package'. $type;
                        $post_data['paymentMethod'] = $pm;
                        $post_data['merchantOrderId'] = $unix;
                        $post_data['customerVaName'] =  $cus_name;
                        $post_data['email'] = $user_info->email;
                        $post_data['phoneNumber'] = $user_info->phone;
                        $post_data['callbackUrl'] = base_url()."duitku_callback";
                        $post_data['returnUrl'] = base_url()."manufacture";
                        $post_data['signature'] = md5($duitku_merchant_code . $unix . $amount . $duitku_merchant_key);
                        $post_data['expiryPeriod'] = '10';
                        $params_string = json_encode($post_data);
                        if($duitku_type == "sandbox") {
                            $api_url = "https://sandbox.duitku.com/webapi/api/merchant/v2/inquiry";
                        }else if($duitku_type == "original") {
                            $api_url = "https://passport.duitku.com/webapi/api/merchant/v2/inquiry";
                        }
    
                        $ch = curl_init();
    
                        curl_setopt($ch, CURLOPT_URL, $api_url); 
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);                                                                  
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                            'Content-Type: application/json',                                                                                
                            'Content-Length: ' . strlen($params_string))                                                                       
                        );   
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                    
                        //execute post
                        $request = curl_exec($ch);
                        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    
                        if($httpCode == 200)
                        {
                            $result = json_decode($request, true);
                            header('location: '. $result['paymentUrl']);
                            echo "paymentUrl :". $result['paymentUrl'] . "<br />";
                            echo "merchantCode :". $result['merchantCode'] . "<br />";
                            echo "reference :". $result['reference'] . "<br />";
                            echo "vaNumber :". $result['vaNumber'] . "<br />";
                            echo "amount :". $result['amount'] . "<br />";
                            echo "statusCode :". $result['statusCode'] . "<br />";
                            echo "statusMessage :". $result['statusMessage'] . "<br />";
                        }else{
                            print_r($request);
                            echo $httpCode;
                        }
                    }
                } else if ($method == 'cash') {
                    $data['manufacture']         = $manufacture;
                    $data['amount']         = $amount;
                    $data['status']         = 'due';
                    $data['method']         = 'cash';
                    $data['timestamp']      = time();
                    $data['membership']     = $type;
                    $this->db->insert('manufacture_membership_payment', $data);
                    redirect(base_url() . 'manufacture/package/', 'refresh');
                } else {
                    echo 'putu';
                }
            } else {
                redirect(base_url() . 'manufacture/package/', 'refresh');
            }
        } else {
            $page_data['page_name'] = "package";
            $this->load->view('back/index', $page_data);
        }
    }

    function manufacture_pum_success()
    {
        $status         =   $_POST["status"];
        $firstname      =   $_POST["firstname"];
        $amount         =   $_POST["amount"];
        $txnid          =   $_POST["txnid"];
        $posted_hash    =   $_POST["hash"];
        $key            =   $_POST["key"];
        $productinfo    =   $_POST["productinfo"];
        $email          =   $_POST["email"];
        $udf1           =   $_POST['udf1'];
        $salt           =   $this->Crud_model->get_settings_value('business_settings', 'pum_merchant_salt', 'value');

        if (isset($_POST["additionalCharges"])) {
            $additionalCharges = $_POST["additionalCharges"];
            $retHashSeq = $additionalCharges.'|'.$salt.'|'.$status.'||||||||||'.$udf1.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
        } else {
            $retHashSeq = $salt.'|'.$status.'||||||||||'.$udf1.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
        }
        $hash = hash("sha512", $retHashSeq);

        if ($hash != $posted_hash) {
            $invoice_id = $this->session->userdata('invoice_id');
            $this->db->where('membership_payment_id', $invoice_id);
            $this->db->delete('manufacture_membership_payment');
            $this->session->set_userdata('invoice_id', '');
            $this->session->set_flashdata('alert', 'payment_cancel');
            redirect(base_url() . 'manufacture/package/', 'refresh');
        } else {

            $data['status']         = 'paid';
            $data['details']        = json_encode($_POST);
            $invoice_id             = $_POST['custom'];
            $this->db->where('membership_payment_id', $invoice_id);
            $this->db->update('manufacture_membership_payment', $data);
            $type = $this->db->get_where('manufacture_membership_payment',array('membership_payment_id'=>$invoice_id))->row()->membership;
            $manufacture = $this->db->get_where('manufacture_membership_payment',array('membership_payment_id'=>$invoice_id))->row()->manufacture;
            $this->crud_model->upgrade_membership($manufacture,$type);
            
            $this->session->set_userdata('invoice_id', '');
            redirect(base_url() . 'manufacture/package/', 'refresh');
        }
    }

    function manufacture_pum_failure()
    {
        $invoice_id = $this->session->userdata('invoice_id');
        $this->db->where('membership_payment_id', $invoice_id);
        $this->db->delete('manufacture_membership_payment');
        $this->session->set_userdata('invoice_id', '');
        $this->session->set_flashdata('alert', 'payment_cancel');
        redirect(base_url() . 'manufacture/package/', 'refresh');
    }

    function manufacture_sslcommerz_success()
    {
        $invoice_id = $this->session->userdata('invoice_id');

        if ($invoice_id != '' || !empty($invoice_id)) {

            $data['status']         = 'paid';
            $data['details']        = json_encode($_POST);

            $this->db->where('membership_payment_id', $invoice_id);
            $this->db->update('manufacture_membership_payment', $data);
            $type = $this->db->get_where('manufacture_membership_payment',array('membership_payment_id'=>$invoice_id))->row()->membership;
            $manufacture = $this->db->get_where('manufacture_membership_payment',array('membership_payment_id'=>$invoice_id))->row()->manufacture;
            $this->crud_model->upgrade_membership($manufacture,$type);
            
            $this->session->set_userdata('invoice_id', '');
            redirect(base_url() . 'manufacture/package/', 'refresh');
        } else {
            redirect(base_url() . 'manufacture/package/', 'refresh');
        }
    }

    function manufacture_sslcommerz_fail()
    {
        $invoice_id = $this->session->userdata('invoice_id');
        $this->db->where('membership_payment_id', $invoice_id);
        $this->db->delete('manufacture_membership_payment');
        $this->session->set_userdata('invoice_id', '');
        $this->session->set_flashdata('alert', 'payment_cancel');
        redirect(base_url() . 'manufacture/package/', 'refresh');
    }

    function manufacture_sslcommerz_cancel()
    {
        $invoice_id = $this->session->userdata('invoice_id');
        $this->db->where('membership_payment_id', $invoice_id);
        $this->db->delete('manufacture_membership_payment');
        $this->session->set_userdata('invoice_id', '');
        $this->session->set_flashdata('alert', 'payment_cancel');
        redirect(base_url() . 'manufacture/package/', 'refresh');
    }
    
    /* FUNCTION: Verify paypal payment by IPN*/
    function paypal_ipn()
    {
        if ($this->paypal->validate_ipn() == true) {
            
            $data['status']         = 'paid';
            $data['details']        = json_encode($_POST);
            $invoice_id             = $_POST['custom'];
            $this->db->where('membership_payment_id', $invoice_id);
            $this->db->update('manufacture_membership_payment', $data);
            $type = $this->db->get_where('manufacture_membership_payment',array('membership_payment_id'=>$invoice_id))->row()->membership;
            $manufacture = $this->db->get_where('manufacture_membership_payment',array('membership_payment_id'=>$invoice_id))->row()->manufacture;
            $this->crud_model->upgrade_membership($manufacture,$type);
        }
    }
    

    /* FUNCTION: Loads after cancelling paypal*/
    function paypal_cancel()
    {
        $invoice_id = $this->session->userdata('invoice_id');
        $this->db->where('membership_payment_id', $invoice_id);
        $this->db->delete('manufacture_membership_payment');
        $this->session->set_userdata('invoice_id', '');
        $this->session->set_flashdata('alert', 'payment_cancel');
        redirect(base_url() . 'manufacture/package/', 'refresh');
    }
    
    /* FUNCTION: Loads after successful paypal payment*/
    function paypal_success()
    {
        $this->session->set_userdata('invoice_id', '');
        redirect(base_url() . 'manufacture/package/', 'refresh');
    }
    
    function twocheckout_success()
    {

        /*$this->twocheckout_lib->set_acct_info('532001', 'tango', 'Y');*/
        $c2_user = $this->db->get_where('business_settings',array('type' => 'c2_user'))->row()->value; 
        $c2_secret = $this->db->get_where('business_settings',array('type' => 'c2_secret'))->row()->value;
        
        $this->twocheckout_lib->set_acct_info($c2_user, $c2_secret, 'Y');
        $data2['response'] = $this->twocheckout_lib->validate_response();
        //var_dump($this->twocheckout_lib->validate_response());
        $status = $data2['response']['status'];
        if ($status == 'pass') {
            $data1['status']             = 'paid';
            $data1['details']   = json_encode($this->twocheckout_lib->validate_response());
            $invoice_id         = $this->session->userdata('invoice_id');
            $this->db->where('membership_payment_id', $invoice_id);
            $this->db->update('manufacture_membership_payment', $data1);
            $type = $this->db->get_where('manufacture_membership_payment',array('membership_payment_id'=>$invoice_id))->row()->membership;
            $manufacture = $this->db->get_where('manufacture_membership_payment',array('membership_payment_id'=>$invoice_id))->row()->manufacture;
            $this->crud_model->upgrade_membership($manufacture,$type);
            redirect(base_url() . 'manufacture/package/', 'refresh');

        } else {
            //var_dump($data2['response']);
            $invoice_id = $this->session->userdata('invoice_id');
            $this->db->where('membership_payment_id', $invoice_id);
            $this->db->delete('manufacture_membership_payment');
            $this->session->set_userdata('invoice_id', '');
            $this->session->set_flashdata('alert', 'payment_cancel');
            redirect(base_url() . 'manufacture/package', 'refresh');
        }
    }
 /* FUNCTION: Verify vouguepay payment by IPN*/
    function vouguepay_ipn()
    {
        $res = $this->vouguepay->validate_ipn();
        $invoice_id = $res['merchant_ref'];
        $merchant_id = 'demo';

        if ($res['total'] !== 0 && $res['status'] == 'Approved' && $res['merchant_id'] == $merchant_id) {
            $data['status']         = 'paid';
            $data['details']        = json_encode($res);
            $this->db->where('membership_payment_id', $invoice_id);
            $this->db->update('manufacture_membership_payment', $data);
        }
    }
    
    /* FUNCTION: Loads after cancelling vouguepay*/
    function vouguepay_cancel()
    {
        $invoice_id = $this->session->userdata('invoice_id');
        $this->db->where('membership_payment_id', $invoice_id);
        $this->db->delete('manufacture_membership_payment');
        $this->session->set_userdata('invoice_id', '');
        $this->session->set_flashdata('alert', 'payment_cancel');
        redirect(base_url() . 'manufacture/package/', 'refresh');
    }
    
    /* FUNCTION: Loads after successful vouguepay payment*/
    function vouguepay_success()
    {
        $this->session->set_userdata('invoice_id', '');
        redirect(base_url() . 'manufacture/package/', 'refresh');
    }
    /* Manage Business Settings */
    function business_settings($para1 = "", $para2 = "")
    {
        if (!$this->crud_model->manufacture_permission('business_settings')) {
            redirect(base_url() . 'manufacture');
        }
        if ($para1 == "cash_set") {
            $val = '';
            if ($para2 == 'true') {
                $val = 'ok';
            } else if ($para2 == 'false') {
                $val = 'no';
            }
            echo $val;
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'cash_set' => $val
            ));
            recache();
        }
        else if ($para1 == "paypal_set") {
            $val = '';
            if ($para2 == 'true') {
                $val = 'ok';
            } else if ($para2 == 'false') {
                $val = 'no';
            }
            echo $val;
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'paypal_set' => $val
            ));
            recache();
        }
        else if ($para1 == "pum_set") {
            $val = '';
            if ($para2 == 'true') {
                $val = 'ok';
            } else if ($para2 == 'false') {
                $val = 'no';
            }
            echo $val;
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'pum_set' => $val
            ));
            recache();
        }
        else if ($para1 == "stripe_set") {
            $val = '';
            if ($para2 == 'true') {
                $val = 'ok';
            } else if ($para2 == 'false') {
                $val = 'no';
            }
            echo $val;
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'stripe_set' => $val
            ));
            recache();
        }
        else if ($para1 == "c2_set") {
            $val = '';
            if ($para2 == 'true') {
                $val = 'ok';
            } else if ($para2 == 'false') {
                $val = 'no';
            }
            echo $val;
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'c2_set' => $val
            ));
            recache();
        }
        else if ($para1 == "vp_set") {
            $val = '';
            if ($para2 == 'true') {
                $val = 'ok';
            } else if ($para2 == 'false') {
                $val = 'no';
            }
            echo $val;
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'vp_set' => $val
            ));
            recache();
        }
        else if ($para1 == "membership_price") {
            echo $this->db->get_where('manufacture_membership',array('membership_id'=>$para2))->row()->price;
        }
        else if ($para1 == "membership_info") {
            $return = '<div class="table-responsive"><table class="table table-striped">';
            if($para2 !== '0'){
                $results = $this->db->get_where('manufacture_membership',array('membership_id'=>$para2))->result_array();
                foreach ($results as $row) {
                    $return .= '<tr>';
                    $return .= '<td>'.translate('title').'</td>';
                    $return .= '<td>'.$row['title'].'</td>';
                    $return .= '</tr>';

                    $return .= '<tr>';
                    $return .= '<td>'.translate('price').'</td>';
                    $return .= '<td>'.currency($row['price'],'def').'</td>';
                    $return .= '</tr>';

                    $return .= '<tr>';
                    $return .= '<td>'.translate('timespan').'</td>';
                    $return .= '<td>'.$row['timespan'].'</td>';
                    $return .= '</tr>';

                    $return .= '<tr>';
                    $return .= '<td>'.translate('maximum_product').'</td>';
                    $return .= '<td>'.$row['product_limit'].'</td>';
                    $return .= '</tr>';
                }
            } else if($para2 == '0'){
                $return .= '<tr>';
                $return .= '<td>'.translate('title').'</td>';
                $return .= '<td>'.translate('default').'</td>';
                $return .= '</tr>';

                $return .= '<tr>';
                $return .= '<td>'.translate('price').'</td>';
                $return .= '<td>'.translate('free').'</td>';
                $return .= '</tr>';

                $return .= '<tr>';
                $return .= '<td>'.translate('timespan').'</td>';
                $return .= '<td>'.translate('lifetime').'</td>';
                $return .= '</tr>';

                $return .= '<tr>';
                $return .= '<td>'.translate('maximum_product').'</td>';
                $return .= '<td>'.$this->db->get_where('general_settings',array('type'=>'default_member_product_limit'))->row()->value.'</td>';
                $return .= '</tr>';
            }
            $return .= '</table></div>';
            echo $return;
        }
        else if ($para1 == 'set') {
            $publishable    = $this->input->post('stripe_publishable');
            $secret         = $this->input->post('stripe_secret');
            $stripe         = json_encode(array('publishable'=>$publishable,'secret'=>$secret));
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'paypal_email' => $this->input->post('paypal_email')
            ));
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'stripe_details' => $stripe
            ));
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'c2_user' => $this->input->post('c2_user'),
                'c2_secret' => $this->input->post('c2_secret'),
            ));
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'vp_merchant_id' => $this->input->post('vp_merchant_id')
            ));
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'pum_merchant_key' => $this->input->post('pum_merchant_key')
            ));
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'pum_merchant_salt' => $this->input->post('pum_merchant_salt')
            ));
            recache();
        } else {
            $page_data['page_name'] = "business_settings";
            $this->load->view('back/index', $page_data);
        }
    }
    

    /* Manage manufacture Settings */
    function manage_manufacture($para1 = "")
    {
        if ($this->session->userdata('manufacture_login') != 'yes') {
            redirect(base_url() . 'manufacture');
        }
        if ($para1 == 'update_password') {
            $user_data['password'] = $this->input->post('password');
            $account_data          = $this->db->get_where('manufacture', array(
                'manufacture_id' => $this->session->userdata('manufacture_id')
            ))->result_array();
            foreach ($account_data as $row) {
                if (sha1($user_data['password']) == $row['password']) {
                    if ($this->input->post('password1') == $this->input->post('password2')) {
                        $data['password'] = sha1($this->input->post('password1'));
                        $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
                        $this->db->update('manufacture', $data);
                        echo 'updated';
                    }
                } else {
                    echo 'pass_prb';
                }
            }
        } else if ($para1 == 'update_profile') {
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'address1' => $this->input->post('address1'),
                'address2' => $this->input->post('address2'),
                'company' => $this->input->post('company'),
                'display_name' => $this->input->post('display_name'),

                'rajaongkir_subdistrict' => $this->input->post('rajaongkir_subdistrict'),
                'rajaongkir_city'        => $this->input->post('rajaongkir_city'),
                'rajaongkir_province'    => $this->input->post('rajaongkir_province'),
                'subdistrict'            => $this->input->post('subdistrict'),
                'city'                   => $this->input->post('city'),
                'state'                  => $this->input->post('state'),
                'zip'                    => $this->input->post('zip'),
             
                'details' => $this->input->post('details'),
                'phone' => $this->input->post('phone'),
                'lat_lang' => $this->input->post('lat_lang')
            ));
        } else {
            $page_data['page_name'] = "manage_manufacture";
            $this->load->view('back/index', $page_data);
        }
    }

    /* Manage General Settings */
    function general_settings($para1 = "", $para2 = "")
    {
        if (!$this->crud_model->manufacture_permission('site_settings')) {
            redirect(base_url() . 'manufacture');
        }

    }

    function site_settings($para1 = "")
    {
        if (!$this->crud_model->manufacture_permission('site_settings')) {
            redirect(base_url() . 'manufacture');
        }

        $page_data['manufacture'] = $this->crud_model->filter_one('manufacture', 'manufacture_id', $this->session->userdata('manufacture_id'));
        $page_data['manufacture'] = array_pop($page_data['manufacture']);
        $page_data['page_name'] = "site_settings";
        $page_data['tab_name']  = $para1;
        $this->load->view('back/index', $page_data);
    }
    
    /* Manage Social Links */
    function social_links($para1 = "")
    {
        if (!$this->crud_model->manufacture_permission('site_settings')) {
            redirect(base_url() . 'manufacture');
        }
        if ($para1 == "set") {

            $rex = '#^https?://#';

            $data['website'] = preg_replace($rex, '', $this->input->post('website'));
            $data['facebook'] = preg_replace($rex, '', $this->input->post('facebook'));
            $data['youtube'] = preg_replace($rex, '', $this->input->post('youtube'));
            $data['instagram'] = preg_replace($rex, '', $this->input->post('instagram'));
            $data['twitter'] = preg_replace($rex, '', $this->input->post('twitter'));

            /*
            skype -> telegram
            pinterest -> whatsapp
            google_plus -> line
            */

            $data['skype'] = $this->input->post('skype');
            $data['pinterest'] = $this->input->post('pinterest');
            $data['google_plus'] = $this->input->post('google-plus');

            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', $data);
            recache();
            redirect(base_url() . 'manufacture/site_settings/social_links/', 'refresh');
        
        }
    }

    /* Manage SEO relateds */
    function seo_settings($para1 = "")
    {
        if (!$this->crud_model->manufacture_permission('site_settings')) {
            redirect(base_url() . 'manufacture');
        }
        if ($para1 == "set") {
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'description' => $this->input->post('description')
            ));
            $this->db->where('manufacture_id', $this->session->userdata('manufacture_id'));
            $this->db->update('manufacture', array(
                'keywords' => $this->input->post('keywords')
            ));
            recache();
        }
    }
    /* Manage Favicons */
    function manufacture_images($para1 = "")
    {
        if (!$this->crud_model->manufacture_permission('site_settings')) {
            redirect(base_url() . 'manufacture');
        }
        move_uploaded_file($_FILES["logo"]['tmp_name'], 'uploads/manufacture_logo_image/logo_' . $this->session->userdata('manufacture_id') . '.png');
        move_uploaded_file($_FILES["banner"]['tmp_name'], 'uploads/manufacture_banner_image/banner_' . $this->session->userdata('manufacture_id') . '.jpg');
        recache();
    }

    function brand($para1 = '', $para2 = '')
    {

        $manufacture_id = $this->session->userdata('manufacture_id');
        if (!$this->crud_model->manufacture_permission('brand')) {
            redirect(base_url() . 'manufacture');
        }
        if ($this->crud_model->get_type_name_by_id('general_settings','86','value') !== 'ok') {
            redirect(base_url() . 'manufacture');
        }
        if ($para1 == 'do_add') {
            $type                = 'brand';
            $data['name']        = $this->input->post('name');
            $data['permission']  = $manufacture_id;
            $this->db->insert('brand', $data);
            $id = $this->db->insert_id();

            $path = $_FILES['img']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $data_banner['logo']         = 'brand_'.$id.'.'.$ext;
            $this->crud_model->file_up("img", "brand", $id, '', 'no', '.'.$ext);
            $this->db->where('brand_id', $id);
            $this->db->update('brand', $data_banner);
            $this->crud_model->set_category_data(0);
            recache();
        } elseif ($para1 == "update") {
            $data['name']        = $this->input->post('name');
            $this->db->where('brand_id', $para2);
            $this->db->where('permission',$manufacture_id);
            $this->db->update('brand', $data);
            if($_FILES['img']['name']!== ''){
                $path = $_FILES['img']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $data_logo['logo']       = 'brand_'.$para2.'.'.$ext;
                $this->crud_model->file_up("img", "brand", $para2, '', 'no', '.'.$ext);
                $this->db->where('brand_id', $para2);
                $this->db->update('brand', $data_logo);
            }
            $this->crud_model->set_category_data(0);
            recache();
        } elseif ($para1 == 'delete') {
            unlink("uploads/brand_image/" .$this->crud_model->get_type_name_by_id('brand',$para2,'logo'));
            $this->db->where('brand_id', $para2);
            $this->db->where('permission',$manufacture_id);
            $this->db->delete('brand');
            $this->crud_model->set_category_data(0);
            recache();
        } elseif ($para1 == 'multi_delete') {
            $ids = explode('-', $param2);
            $this->crud_model->multi_delete('brand', $ids);
        } else if ($para1 == 'edit') {
            $page_data['brand_data'] = $this->db->get_where('brand', array(
                'brand_id' => $para2
            ))->result_array();
            $this->load->view('back/manufacture/brand_edit', $page_data);
        } elseif ($para1 == 'list') {
            $this->db->order_by('brand_id', 'desc');
            $this->db->where('permission',$manufacture_id);
            $page_data['all_brands'] = $this->db->get('brand')->result_array();
            $this->load->view('back/manufacture/brand_list', $page_data);
        } elseif ($para1 == 'add') {
            $this->load->view('back/manufacture/brand_add');
        } else {
            $page_data['page_name']  = "brand";
            $page_data['all_brands'] = $this->db->get('brand')->result_array();
            $this->load->view('back/index', $page_data);
        }
    }


    function sub_category($para1 = '', $para2 = '')
    {
        if (!$this->crud_model->manufacture_permission('sub_category')) {
            redirect(base_url() . 'manufacture');
        }
        if ($this->crud_model->get_type_name_by_id('general_settings','68','value') !== 'ok') {
            redirect(base_url() . 'manufacture');
        }
        if ($para1 == 'edit') {
            $page_data['sub_category_data'] = $this->db->get_where('sub_category', array(
                'sub_category_id' => $para2
            ))->result_array();
            $this->load->view('back/manufacture/sub_category_edit', $page_data);
        } elseif ($para1 == "update") {
            $data['category']          = $this->input->post('category');
            $existing_brands = $this->db->get_where('sub_category', array('sub_category_id' => $para2))->row()->brand;
            $existing_brands = json_decode($existing_brands, true);

            $id_brand = $this->db->get_where('brand', array('permission' => $this->session->userdata('manufacture_id')))->result_array();
            foreach($id_brand as $didi){
                $didis[] = $didi['brand_id'];
            }

            $kloj = array_diff($existing_brands, $didis);
            if($this->input->post('brand')==NULL)
            {
                $data['brand']             = json_encode($kloj);
            }
            else{
                $data['brand']             = json_encode(array_unique(array_merge($kloj, $this->input->post('brand'))));
            }

            $this->db->where('sub_category_id', $para2);
            $this->db->update('sub_category', $data);
            
            if($_FILES['img']['name']!= ''){
                $path = $_FILES['img']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $data_banner['banner']       = 'sub_category_'.$para2.'.'.$ext;
                $this->crud_model->file_up("img", "sub_category", $para2, '', 'no', '.'.$ext);
                $this->db->where('sub_category_id', $para2);
                $this->db->update('sub_category', $data_banner);
            }
            $this->crud_model->set_category_data(0);
            recache();
        } elseif ($para1 == 'list') {
            $this->db->order_by('sub_category_id', 'desc');
            $this->db->where('digital=',NULL);
            $page_data['all_sub_category'] = $this->db->get('sub_category')->result_array();
            $this->load->view('back/manufacture/sub_category_list', $page_data);
        } else {
            $page_data['page_name']        = "sub_category";
            $page_data['all_sub_category'] = $this->db->get('sub_category')->result_array();
            $this->load->view('back/index', $page_data);
        }
    }

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */